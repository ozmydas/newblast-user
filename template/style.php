<style type="text/css">
  .header.dark-bg{
    background: #141E30;
    background: -webkit-linear-gradient(to right, #243B55, #141E30);
    background: linear-gradient(to right, #243B55, #141E30);
  }

  .modal-dialog.modal-rounded .modal-content{
    border-radius: 10px !important;
    overflow: hidden;
  }

  .form-validate .form-group label[for="image"].error, .form-validate .form-group label[for="ktp"].error{
    background: #00A8E1 !important;
    z-index: 99;
    display: inline-block;
    margin-top: 30px !important;
    color: #fff !important; 
    padding: 3px 5px 0 5px;
  }

  .service-container > div{
    position: relative;
    height: 150px;
    overflow: hidden;
    margin-bottom: 10px;
    /*background: pink;*/
    padding: 0 10px !important;
    cursor: pointer;
  }

  .img-container{
    overflow: hidden;
  }

  .service-container img{
    width: 100%;
    transition: all 1s;
  }

  .service-item{
    margin-bottom: 10px;
    position: absolute;
    top: 0;
    bottom: -17px;
    left: 10px;
    right: 10px;
    text-align: center;
    padding-top: 7px;
    padding-bottom: 7px;
    font-size: 1.5em;
    display: flex;
    flex-direction: column;
  }

  .service-item #atas{
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .service-item #bawah{
    background: rgba(0,0,0,0.7);
    transition: all 0.6s;
    color: #eee;
  }

  .service-item #atas i{
    background: rgba(0,0,0,0.7);
    width: 70px;
    height: 70px;
    font-size: 2em;
    line-height: 73px;
    border-radius: 50%;
    color: #eee;
    transition: all 0.6s;
  }

  .service-container > div:hover div img{
    transform: scale(1.2);
  }

  .service-container > div:hover .service-item #atas i{
    background: rgba(254,180,102, 0.9);
    color: rgba(0,0,0,0.8);
  }

  .service-container > div:hover .service-item #bawah{
    background: rgba(254,180,102, 0.9);
    color: rgba(0,0,0,0.8);
  }

  .animate-order-1{
    animation-delay: 0.3s;
  }

  .animate-order-2{
    animation-delay: 0.6s;
  }

  .animate-order-3{
    animation-delay: 0.9s;
  }

  .animate-order-4{
    animation-delay: 1.2s;
  }

  .animate-order-5{
    animation-delay: 1.5s;
  }

  .animate-order-6{
    animation-delay: 1.8s;
  }

  .animate-order-7{
    animation-delay: 2.1s;
  }

  .animate-order-8{
    animation-delay: 2.4s;
  }

  .animate-order-9{
    animation-delay: 2.7s;
  }

  .animate-order-10{
    animation-delay: 3s;
  }
</style>