<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">My Message</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="#">Home</a></li>
        <li>Pesan</li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Kotak Masuk
          
          <span style="float: right;">
            <button style="padding-top: 4px; padding-bottom: 4px; margin-top: 3px; font-weight: bold;" class="btn btn-sm btn-primary" id="btnReload"><span class="fa fa-refresh"></span> Reload</button>
            <button style="padding-top: 4px; padding-bottom: 4px; margin-top: 3px; font-weight: bold;" class="btn btn-sm btn-primary" data-toggle="modal" href="#myModal" data-backdrop="static" data-keyboard="false"><span class="fa fa-edit"></span> Tulis Pesan</button>
          </span>
        </header>
        <div class="panel-body">
          <!--  -->

          <!-- DISINI TABLE NYA -->
          <table id="myTable" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Pengirim</th>
                <th class="text-center">Subjek</th>
                <th class="text-center">Status</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          <!-- SAMPAI SINI -->

          <!--  -->
        </div>
      </section>
    </div>
  </div>
  <!-- page end-->
</section>


<!-- MODAL TULIS -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-rounded">
    <div class="modal-content" style="">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lengkapi Form Berikut</h4>
      </div>
      <div class="modal-body">

        <form action="" method="post" role="form" id="myForm" class="contactForm">

          <label>Penerima</label>
          <div class="form-group">
            <input type="text" name="name" class="form-control clean" id="name" placeholder="Penerima" value="Musicblast.id" data-rule="minlen:4" data-msg="Please enter at least 4 chars" readonly />
            <div class="validation"></div>
          </div>
          
          <label>Subjek</label>
          <div class="form-group">
            <input type="text" class="form-control clean" name="subject" id="subject" placeholder="Isi Subjek" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
            <div class="validation"></div>
          </div>

          <label>Pesan</label>
          <div class="form-group">
            <textarea class="form-control" name="message" id="message" rows="3" data-rule="required" data-msg="Please write something for us" placeholder="Isi Pesan"></textarea>
            <div class="validation"></div>
          </div>

        </form>

      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">Batal</button>
        <button class="btn btn-success" type="button" id="btnSend"><span class="fa fa-send"></span> Kirim Pesan</button>
      </div>
    </div>
  </div>
</div>
<!-- modal -->


<!-- MODAL REPLY -->
<div class="modal fade" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-rounded">
    <div class="modal-content" style="">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Balas Pesan</h4>
      </div>
      <div class="modal-body">

        <form action="" method="post" role="form" id="replyForm" class="contactForm">

          <label>Penerima</label>
          <div class="form-group">
            <input type="hidden" id="id_pesan" name="id_pesan" value="<?=@$opened_message['id_pesan']?>">
            <input type="hidden" id="recipient" name="recipient" value="<?=@$opened_message['id_pengirim']?>">
            <input type="text" name="name" class="form-control clean" id="name" placeholder="Penerima" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="<?=@$opened_message['name']?>" readonly />
            <div class="validation"></div>
          </div>
          
          <label>Subjek</label>
          <div class="form-group">
            <input type="text" class="form-control clean" name="tarsubject" id="subject" placeholder="Isi Subjek" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" value="<?=@$opened_message['subyek_pesan']?>" readonly/>
            <div class="validation"></div>
          </div>

          <label>Pesan</label>
          <div class="form-group">
            <textarea class="form-control" name="tarmessage" id="message" rows="3" data-rule="required" data-msg="Please write something for us" placeholder="Isi Pesan" readonly><?=@$opened_message['isi_pesan']?></textarea>
            <div class="validation"></div>
          </div>

          <div style="width: 100%; background: teal; color: #fff; padding-top: 10px; padding-bottom: 10px; cursor: pointer; text-align: center;" data-toggle="collapse" data-target="#kolappesan"><i class="fa fa-chevron-down"></i> Balas Pesan</div>

          <div id="kolappesan" class="collapse">
            <br/>
            <label>Pesan Balasan</label>
            <div class="form-group">
              <textarea class="form-control clean" name="message" id="repmessage" rows="3" data-rule="required" data-msg="Please write something for us" placeholder="Isi Pesan"></textarea>
            </div>
            <div>
              <span class="btn btn-success" id="btnReply"><span class="fa fa-send"></span> KIRM BALASAN</span>
            </div>
          </div>

        </form>

      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">Batal</button>
      </div>
    </div>
  </div>
</div>
<!-- modal -->


<script type="text/javascript">
  $('#kolappesan').collapse('hide');

  <?php if($opened_message != FALSE): ?>
    $('#replyModal').modal('show');
  <?php endif; ?>

  $(document).ready(function() {
    table = $('#myTable').DataTable( {
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url" : "<?=$site_url?>proses/message/pesanlist.php",
        "type": "POST"
      },
      'lengthChange': true,
      'autoWidth'   : false,
      "order": [],
      'scrollX'  : true,
      "columnDefs": [
      { 
        "targets": [1,3,4],
        "class": "text-center",
      },
      { 
        "targets": [4],
        "orderable": false,
      },
      ], 
    });
  });

  function reload_table(){
    $('#myTable').DataTable().ajax.reload();
  }
</script>

<script type="text/javascript">

  $('#btnSend').on('click', function(e){
    e.preventDefault();

    loading();

    var uri = '<?=$site_url?>proses/message/send.php';
    var formData = new FormData($('#myForm')[0]);

    $.ajax({
      type: "POST",
      url: uri,
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response){
        console.log(response);
        if( ! response.status){                 
          Swal.fire({
            type: 'warning',
            text: response.message,
          })
        } else{
          $('[data-dismiss="modal"]').click();
          $('.clean').val('');

          Swal.fire({
            type: 'success',
            text: response.message,
          })
        }
      }, 
      error: function(err){
        console.log(err);
        Swal.fire({
          type: 'error',
          text: err.status + " : " + err.statusText,
        })
      }
    });
  })


  $('#btnReply').on('click', function(e){
    e.preventDefault();

    if($('#replyForm [name="message"]').val() == ""){
      Swal.fire({
        type: 'warning',
        text: 'Pesan Balasan Tidak Boleh Kosong!'
      });

      return false;
    }

    loading();

    var uri = '<?=$site_url?>proses/message/send.php';
    var formData = new FormData($('#replyForm')[0]);
    var strRep = $('#replyForm #subject').val();
    formData.append('subject', 'REPLY : ' + strRep.replace("REPLY : ", ""));

    $.ajax({
      type: "POST",
      url: uri,
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response){
        console.log(response);
        if( ! response.status){                 
          Swal.fire({
            type: 'warning',
            text: response.message,
          })
        } else{
          $('[data-dismiss="modal"]').click();
          $('.clean').val('');

          Swal.fire({
            type: 'success',
            text: response.message,
          })
        }
      }, 
      error: function(err){
        console.log(err);
        Swal.fire({
          type: 'error',
          text: err.status + " : " + err.statusText,
        })
      }
    });
  })


</script>

<script type="text/javascript">
  function bacapesan(id){
    console.log(id);

    loading();
    $('#kolappesan').collapse('hide');

    var uri = '<?=$site_url?>proses/message/bacapesan.php?id_pesan=' + id;

    $.ajax({
      type: "POST",
      url: uri,
      data: null,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response){
        console.log(response);
        if( ! response.status){                 
          Swal.fire({
            type: 'warning',
            text: response.message,
          })
        } else{
          console.log(response.data)
          var data = response.data;

          $('#replyForm #id_pesan').val(data.id_pesan);
          $('#replyForm #recipient').val(data.id_pengirim);
          $('#replyForm #subject').val(data.subyek_pesan);
          $('#replyForm #message').val(data.isi_pesan);
          $('#replyForm #name').val(data.pengirim);

          Swal.close();
          reload_table();
          $('#replyModal').modal('show');
        }
      }, 
      error: function(err){
        console.log(err);
        Swal.fire({
          type: 'error',
          text: err.status + " : " + err.statusText,
        })
      }
    });
  }

  $('#btnReload').on('click', function(){
    window.location.replace("<?=$site_url?>index.php?message");
  })
</script>