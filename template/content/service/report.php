<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">Report</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="#">Home</a></li>
        <li>Report</li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Request Laporan Digital
        </header>
        <div class="panel-body text-center">
          <!--  -->

          <h1>Request Digital</h1>
          <p>"Silahkan Kirim Email ke Musicblastindonesia@gmail.com dengan Subyek : Request Report, Isi Email : Nama Artist dan Judul Lagu"</p>
          
          <hr style="width: 70%" />
          
          <p>Atau Kamu Bisa Langsung Mengirim Request Melalui Tombol Dibawah :</p>
          
          <button class="btn btn-success animated fadeIn" style="font-size: 1.5em; padding-left: 30px; padding-right: 30px; border-radius: 10px 0 10px 0; font-weight: bold; box-shadow: 1px 1px 5px #aaa" data-toggle="modal" href="#myModal" data-backdrop="static" data-keyboard="false">
            KIRIM REQUEST
          </button>
          <!--  -->
        </div>
      </section>
    </div>
  </div>

  <!-- page end-->
</section>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-rounded">
    <div class="modal-content" style="">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Request Report</h4>
      </div>
      <div class="modal-body">

        <form action="" method="post" role="form" id="myForm" class="contactForm">

          <label>Email</label>
          <div class="form-group">
            <input type="text" name="email" class="form-control clean" id="email" placeholder="Email Kamu" value="<?=$userdata['email']?>" data-rule="minlen:4" data-msg="Please enter at least 4 chars" readonly />
          </div>

          <label>Nama Artis/Judul Lagu</label>
          <div class="form-group">
            <select name="songs[]" class="form-control select2" multiple="multiple" required>
              <!-- <option value="" selected disabled>Pilih Lagu</option> -->
              <?php foreach($songs_available as $key => $value): ?>
                <option value="<?=$value['value']?>"><?=$value['name']?></option>
              <?php endforeach; ?>
            </select>
          </div>

        </form>

      </div>

      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">Batal</button>
        <button class="btn btn-success" type="button" id="btnSend"><span class="fa fa-send"></span> Kirim Request</button>
      </div>
    </div>
  </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>


<script type="text/javascript">
  $(document).ready(function() {
    $('.select2').select2({
      placeholder: "Pilih Lagu dari Single / Album",
      allowClear: true
    });
  });

  $('#btnSend').on('click', function(e){
    e.preventDefault();

    loading();

    var uri = '<?=$site_url?>proses/service/reportrequest.php';
    var formData = new FormData($('#myForm')[0]);

    $.ajax({
      type: "POST",
      url: uri,
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response){
        console.log(response);
        if( ! response.status){                 
          Swal.fire({
            type: 'warning',
            text: response.message,
          })
        } else{
          $('[data-dismiss="modal"]').click();
          $('#message-req').val(`NAMA ARTIS :

            JUDUL LAGU : `);

          Swal.fire({
            type: 'success',
            text: response.message,
          })
        }
      }, 
      error: function(err){
        console.log(err);
        Swal.fire({
          type: 'error',
          text: err.status + " : " + err.statusText,
        })
      }
    });
  })

</script>

<style type="text/css">
  .select2-container--default.select2-container--focus .select2-selection--multiple{
    border: 1px solid #007AFF;
  }


  .select2-container--default .select2-search--inline .select2-search__field{
    width: 200% !important;
  }

  .select2-container--default .select2-selection--single{
    min-height: 34px !important;
  }

  .select2-container--default .select2-selection--single .select2-selection__rendered{
    line-height: 34px !important;
  }
  
  .select2.select2-container.select2-container--default{
    width: 100% !important;
    min-height: 34px !important;
  }

</style>