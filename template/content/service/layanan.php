<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">Promotion Service</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=$site_url?>index.php?dashboard">Home</a></li>
        <li><a href="<?=$site_url?>index.php?service"><?=$page_title?></a></li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <?=$subtitle?>
        </header>
        <div class="panel-body">
          <!--  -->
          <div class="row service-container" id="">

            <?php $i=0;
            foreach($services as $key => $value): ?>
              <div class="col-sm-12 col-md-6 col-lg-4 clickable animated fadeIn animate-order-<?=$i++?>" data-target="<?=$value["link"]?>">
                <div class="img-container">
                  <img src="<?=$value['img']?>"/>
                </div>
                <div class="service-item">
                  <div id="atas"><i class="<?=$value["icon"]?>"></i></div>
                  <div id="bawah"><?=$value["name"]?></div>
                </div>
              </div>
            <?php endforeach; ?>

          </div>
          <!--  -->
        </div>
      </section>
    </div>
  </div>

  <!-- page end-->
</section>

<script type="text/javascript">
  $('.clickable').on('click', function(){
    var uri = $(this).attr('data-target');
    window.location.replace(uri);
  })
</script>