<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">Promotion Service</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="#">Home</a></li>
        <li>Promotion</li>
        <li>Store List</li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Store List
        </header>
        <div class="panel-body">
          <!--  -->

          <!-- DISINI TABLE NYA -->
          <!-- <table id="myTable" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th class="text-center">Nama Store</th>
                <th class="text-center">Deskripsi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($stores as $key => $value): ?>
                
                <tr>
                  <td><?=$value["name"]?></td>
                  <td><?=$value["description"]?></td>
                </tr>

              <?php endforeach; ?>
            </tbody>
          </table> -->
          <!-- SAMPAI SINI -->

          <div class="row animated fadeInUp">
            
            <?php foreach($stores as $key => $value): ?>
            <div class="col-lg-3 col-md-4 col-sm-6" style="border: 1px solid #ccc">
              <a href="<?=$value['link']?>" target="_blank">
              <div style="height: 150px; display: flex; justify-content: center; align-items: center;">
                <img src="<?=$value['img']?>" class="img img-responsive">
              </div>
            </a>
            </div>
          <?php endforeach; ?>

          </div>


          <!--  -->
        </div>
      </section>
    </div>
  </div>

  <!-- page end-->
</section>

<script type="text/javascript">
  $(document).ready(function() {
    table = $('#myTable').DataTable();
  });
</script>