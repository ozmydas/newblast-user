<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">Promotion Service</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="#">Home</a></li>
        <li>Promotion</li>
        <li>Service Items</li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Service Item
        </header>
        <div class="panel-body">
          <!--  -->

          <!-- DISINI TABLE NYA -->
          <table id="myTable" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th class="text-center">Service</th>
                <th class="text-center">Deskripsi</th>
                <th class="text-center">Harga</th>
                <th class="text-center">Status</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($stores as $key => $value): ?>
                
                <tr>
                  <td><?=$value["name"]?></td>
                  <td><?=$value["description"]?></td>
                  <td><?=$value["price"]!=""?$value["price"]:'<label class="label" style="background: orange"><i class="fa fa-phone" style="margin-right: 5px;"></i> CALL US</label>'?></td>
                  <td><?=$value["status"]?></td>
                </tr>

              <?php endforeach; ?>
            </tbody>
          </table>
          <!-- SAMPAI SINI -->

          <!--  -->
        </div>
      </section>
    </div>
  </div>

  <!-- page end-->
</section>

<script type="text/javascript">
  $(document).ready(function() {
    table = $('#myTable').DataTable();
  });
</script>