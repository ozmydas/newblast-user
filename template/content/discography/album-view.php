<?php
$_SESSION['INPUT_FORM'] = [
  'input_id' => base64_encode(sha1(date('Ymd').mt_rand()))
];
?>

<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">Lihat Album</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="index.php?dashboard">Home</a></li>
        <li>Discography</li>
        <li>Album</li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Lihat Album Kamu

          <a href="<?=$site_url?>index.php?album" style="float: right; padding-top: 4px; padding-bottom: 4px; margin-top: 3px; font-weight: bold;" class="btn btn-sm btn-primary"><span class="fa fa-chevron-left"></span> Kembali</a>
        </header>
        <div class="panel-body">
          <!--  -->

          <div class="row">
            <div class="col-lg-12">
              <div class="alert alert-warning text-center hide-edit" style="color: orange; display: none; ">
                Silahkan Edit Informasi Album.<br/>Untuk Cover dan File Lagu, Tidak Bisa Diubah.
              </div>

              <!--  -->
              <div class="form">
                <form class="form-validate form-horizontal " id="submitForm"  action="" method="post" enctype="multipart/form-data">

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Judul Album <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <input type="hidden" name="<?=$_SESSION['INPUT_FORM']['input_id']?>" value="<?=$_GET['id']?>">
                      <input class=" form-control editable" id="title" name="title" type="text" value="<?=$album["album_name"]?>" disabled required />
                    </div>
                  </div>

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Artist <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <input class=" form-control editable" id="artist" name="artist" type="text"  value="<?=$album["artist"]?>" disabled required />
                    </div>
                  </div>

                  <!-- <div class="form-group ">
                    <label class="control-label col-lg-2">Feat Artist </label>
                    <div class="col-lg-9 col-md-12">
                      <input class=" form-control" id="ft_artist" name="ft_artist" type="text" value="<?=$album["ft_artist"]?>" disabled />
                    </div>
                  </div>

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Bahasa <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <input class=" form-control" id="bhsa" name="bhsa" type="text"  value="<?=$album["bhsa"]?>" disabled  />
                    </div>
                  </div> -->

                  <!-- <div class="form-group ">
                    <label class="control-label col-lg-2">Genre <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <select class="form-control" type="text" name="genres" id="genres" readonly>
                       <option value="" selected disabled>-- Select Genre --</option>
                       <?php 
                       foreach($list_genre as $g){
                         ?>
                         <option value="<?php echo $g['id']; ?>" <?=$g['id']==$album['genre_id']?'selected':'';?> ><?php echo $g['name']; ?></option>
                         <?php
                       }
                       ?>
                     </select>

                     <small>Jenis aliran musik yang utama</small>
                   </div>
                 </div> -->

                 <div class="form-group">
                  <label class="control-label col-lg-2">Tanggal Rilis <span class="required">*</span></label>
                  <?php
                  date_default_timezone_set("Asia/Jakarta");
                  $date_now = date("Y-m-d");
                  $date_plus_14 = date("Y-m-d", strtotime('+14 days', strtotime($date_now)));
                  ?>

                  <div class="col-lg-9 col-md-12">
                    <input class="form-control" type="date" name="tgl_release" value="<?php echo $album['tgl_release']; ?>" readonly />
                    <small>Tanggal mulainya rilis penjualan lagu (Minimal 14 hari setelah single ditambahkan)</small>
                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Deskripsi Lagu </label>
                  <div class="col-lg-9 col-md-12">
                    <textarea class=" form-control editable" id="deskripsi" name="deskripsi" type="text" disabled><?=$album["desc_album"]?></textarea>
                  </div>
                </div>

                <!-- <div class="form-group ">
                  <label class="control-label col-lg-2">Lirik Lagu <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <textarea class=" form-control" id="song_lyric" name="song_lyric" type="text" disabled >
                      <?=$album["lirik"]?>
                    </textarea>
                  </div>
                </div> -->


                <div class="form-group ">
                  <label class="control-label col-lg-2">Contact Person <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control editable" id="cp" name="cp" type="text"  value="<?=$album["phone"]?>" disabled required />
                  </div>
                </div>


                <!-- <div class="form-group ">
                  <label class="control-label col-lg-2">Akun Instagram</label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control" id="ig" name="ig" type="text" value="<?=$album["ig"]?>" disabled />
                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Akun Facebook</label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control" id="fb" name="fb" type="text" value="<?=$album["fb"]?>" disabled />
                  </div>
                </div>
              -->

              <div class="form-group ">
                <label class="control-label col-lg-2">Label</label>
                <div class="col-lg-9 col-md-12">
                  <!-- <input type="checkbox" class="lbl" name="label" value="" <?= $value['label']!='musicblast.id'?'checked':'' ?>> Custom Label (Jika memakai nama label sendiri berbayar Rp. 200.000)<br> -->
                  <input class="form-control" type="text" class="lblnm" name="lebelname" value="<?=$album["label"]?>" disabled/>
                </div>
              </div>


              <div class="form-group ">
                <label class="control-label col-lg-2">Pencipta Lagu  <span class="required">*</span></label>
                <div class="col-lg-9 col-md-12">
                  <input class=" form-control editable" id="penulis" name="penulis" type="text"  value="<?=$album["pnlis"]?>" disabled required />
                </div>
              </div>

              <div class="form-group ">
                <label class="control-label col-lg-2">Komposer  <span class="required">*</span></label>
                <div class="col-lg-9 col-md-12">
                  <input class=" form-control editable" id="komposer" name="komposer" type="text"  value="<?=$album["kmposer"]?>" disabled required />
                </div>
              </div>

              <div class="form-group ">
                <label class="control-label col-lg-2">Tahun Produksi  <span class="required">*</span></label>
                <div class="col-lg-9 col-md-12">
                  <input class=" form-control editable" id="thn_prdksi" name="thn_prdksi" type="text"  value="<?=$album["thn_prodksi"]?>" disabled required />
                </div>
              </div>

              <div class="form-group file-view">
                <label class="control-label col-lg-2">Cover <span class="required">*</span></label>
                <div class="col-lg-9 col-md-12">
                  <img class="img img-responsive" style="height: 150px; width: auto;" src="https://drive.google.com/uc?export=view&id=<?=$album['cover_album']?>">
                </div>
              </div>

              <div class="form-group file-view">
                <label class="control-label col-lg-2">Song <span class="required">*</span></label>
                <div class="col-lg-9 col-md-12">

                  <?php
                  if(empty($album['songs'])){
                    echo "Tidak ada lagu.";
                  }else{
                    $song = explode(',',$album['songs']);
                    $num_song = sizeof($song);;
                    $n = 1;
                    echo '<div class="row">';
                    for($i=0; $i < $num_song; $i++){
                      ?>
                      <div class="col-sm-12 col-md-6 col-lg-6" style="margin-top: 10px;">
                        <small><?php echo $n++ . '. ' . $song[$i]; ?></small> <br>
                        <audio controls>
                          <source src="https://drive.google.com/uc?export=view&id=<?php echo $song[$i]; ?>" type="audio/mpeg">
                            Your browser does not support the audio element.
                          </audio>
                        </div>
                        <?php
                      }
                      echo "</div>";
                    }
                    ?>
                    <br/><br/>

                  </div>
                </div>


                <div class="form-group file-view">
                  <label class="control-label col-lg-2">KTP <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <!-- <input class="dropify" data-height="150" id="image" name="image" type="file" required /> -->
                    <!-- <small>Mohon masukkan gambar dengan resolusi 1440x1440 px , High Resolusion/TIdak Blur</small> -->
                    <img class="img img-responsive" style="height: 150px; width: auto;" src="https://drive.google.com/uc?export=view&id=<?=$album['ktp']?>">
                  </div>
                </div>


                <div class="form-group  file-edit">
                  <label class="control-label col-lg-2">Cover</label>
                  <div class="col-lg-9 col-md-12">
                    <input class="dropify" data-height="150" id="image" name="image" type="file" data-default-file="https://drive.google.com/uc?export=view&id=<?=$album['cover_album']?>"/>
                    <small>Mohon masukkan gambar dengan resolusi 1440x1440 px , High Resolusion/TIdak Blur</small>
                  </div>
                </div>

                <div class="form-group  file-edit">
                  <label class="control-label col-lg-2">Song</label>
                  <div class="col-lg-9 col-md-12">
                    <input class="form-control" type="file" name="song[]" accept="audio/x-wav" multiple="" />
                    <small>Masukan Lagu Dengan Format WAV Untuk Mengubah Lagu. Biarkan kosong jika tetap memakai lagu lama</small>
                  </div>
                </div>

                <div class="form-group  file-edit">
                  <label class="control-label col-lg-2">KTP</label>
                  <div class="col-lg-9 col-md-12">
                    <input class="dropify" data-height="150" id="ktp" name="ktp" type="file" accept="image/jpeg, image/png" data-default-file="https://drive.google.com/uc?export=view&id=<?=$album['ktp']?>"/>
                    <small>Mohon Masukkan KTP Penanggung Jawab (format JPG/PNG)</small>
                  </div>
                </div>

                  <!-- <div class="form-group ">
                    <label class="control-label col-lg-2">Store <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <input type="checkbox" name="store[]" value="All Store (7-14 hari kerja)" checked> All Store (7-14 hari kerja)<br>
                      <input type="checkbox" name="store[]" value="Joox (45 hari Kerja)" > Joox (45 hari Kerja)<br>
                      <input type="checkbox" name="store[]" value="Youtube Service/Fingerprint" > Youtube Service/Fingerprint<br>
                      <input type="checkbox" name="store[]" value="Local Store" > Local Store (RBT, Melon Musik, Langit Musik. 45 hari Kerja)  Berbayar Rp. 350.000<br>
                    </div>
                  </div> -->

                  <!-- <div class="form-group ">
                    <label class="control-label col-lg-2">Store  <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">

                      <?php
                      $stores = explode(',', $album['store']);
                      foreach ($stores as $key => $value): ?>
                        <?=$value?>
                      <?php endforeach; ?>
                      
                    </div>
                  </div> -->

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Status</label>
                    <div class="col-lg-9 col-md-12">
                      <input class=" form-control" id="status" name="status" type="text"  value="<?=$album["status_album"]?>" disabled  />
                    </div>
                  </div>

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Keterangan</label>
                    <div class="col-lg-9 col-md-12">
                      <textarea class=" form-control" id="keterangan" name="keterangan" disabled><?=$album["Kterangan"]?></textarea>
                    </div>
                  </div>

                  <!-- <div class="form-group ">
                    <label for="agree" class="control-label col-lg-2 col-sm-3"></label>
                    <div class="col-lg-9 col-md-12 col-sm-9">
                      <span class="required">*</span> Setuju dan Telah Membaca <a href="index.php?sk" target="_blank">Syarat dan Ketentuan</a>
                      <input type="checkbox" style="width: 20px" class="checkbox form-control" id="agree" name="agree" />
                      <br>
                      <span class="required">*</span> Pastikan mengisi dengan benar, data yang telah dikonfirmasi tidak dapat diubah.
                    </div>
                  </div> -->

                </form>

                <hr>

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-9 col-md-12">
                    <!-- <button class="btn btn-primary" type="submit" name="submit">Save</button> -->
                    <a href="<?=$site_url?>index.php?album" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali ke Album</a>

                    <?php if($album['status_album'] == "Belum Terkonfirmasi"): ?>
                      <span class="btn btn-success" id="btnEdit"><i class="fa fa-edit"></i> Edit Album</span>
                      <span class="btn btn-warning" id="btnUpdate" style="display: none;"><i class="fa fa-edit"></i> Update Album</span>
                    <?php endif; ?>

                  </div>
                </div>
              </div>
              <!--  -->

            </div>
          </div>

          <!--  -->
        </div>
      </section>
    </div>
  </div>

  <!-- page end-->
</section>



<?php  include('album-js-edit.php'); ?>