<style type="text/css">
	.file-edit{
		display: none;
	}
</style>

<script type="text/javascript">

  $('.dropify').dropify();

	$('#btnEdit').on('click', function(){
		$('.editable').attr("disabled", false);
		$('#btnEdit').css('display', 'none');
		$('#btnUpdate').fadeIn();
		$('.hide-edit').fadeIn();
		$('html, body').animate({scrollTop: 0})


		$('.file-edit').fadeIn();
		$('.file-view').fadeOut();
	})

	/****/

	$('#btnUpdate').on('click', function(){
		$("#submitForm").submit();
	})


	$("#submitForm").submit(function(e) {
		console.log(e);
		e.preventDefault();
	}).validate({
		rules: {
			title: {
				required: true,
				minlength: 2
			},

			artist: {
				required: true,
				minlength: 2
			},

			// bhsa: {
			// 	required: true,
			// 	minlength: 2
			// },

			// genres: {
			// 	required: true,
			// },

			// song_lyric: {
			// 	required: true,
			// 	minlength: 2
			// },
			cp: {
				required: true,
				minlength: 2
			},
			// image: {
			// 	required: true,
			// 	minlength: 2
			// },
			penulis: {
				required: true,
				minlength: 2
			},
			komposer: {
				required: true,
				minlength: 2
			},
			thn_prdksi: {
				required: true,
				minlength: 4,
				maxlength: 4
			},
			// store: {
			// 	required: true,
			// },
			// agree: "required"
		},
		submitHandler: function(form) {
			loading();

			var uri = '<?=$site_url?>proses/discography/albumupdate.php';
			var formData = new FormData($('#submitForm')[0]);

			$.ajax({
				type: "POST",
				url: uri,
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function(response){
					console.log(response);
					if( ! response.status){                 
						Swal.fire({
							type: 'warning',
							text: response.message,
						})
					} else{
						Swal.fire({
							type: 'success',
							text: response.message,
						})

						// balikin seperti semula
						$('.editable').attr("disabled", true);
						$('#btnEdit').fadeIn();
						$('#btnUpdate').css('display', 'none');
						$('.hide-edit').fadeOut();

						$('.file-edit').fadeOut();
						$('.file-view').fadeIn();

						setTimeout(function(){
							window.location.replace('<?=$site_url?>index.php?view_album&id=<?=$_GET['id']?>');
						}, 700);
					}
				},
				error: function(err){
					console.log(err);
					Swal.fire({
						type: 'error',
						text: err.status + " : " + err.statusText,
					})
				}
			});

			return false;
		}
	});


</script>