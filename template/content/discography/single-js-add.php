
<script type="text/javascript">
  $('.dropify').dropify();
</script>

<script type="text/javascript">
    // validate signup form on keyup and submit
    $("#submitForm").submit(function(e) {
      e.preventDefault();
    }).validate({
      rules: {
        title: {
          required: true,
          minlength: 2
        },

        artist: {
          required: true,
          minlength: 2
        },

        bhsa: {
          required: true,
          minlength: 2
        },

        genres: {
          required: true,
        },

        song_lyric: {
          required: true,
          minlength: 2
        },
        cp: {
          required: true,
          minlength: 2
        },
        image: {
          required: true,
          minlength: 2
        },
        penulis: {
          required: true,
          minlength: 2
        },
        komposer: {
          required: true,
          minlength: 2
        },
        thn_prdksi: {
          required: true,
          minlength: 4,
          maxlength: 4
        },
        song: {
          required: true,
          minlength: 2
        },
        store: {
          required: true,
        },
        agree: "required"
      },
      messages: {                
        title: {
          required: "Judul Wajib Diisi.",
          minlength: "Minimal 2 character."
        },               
        artist: {
          required: "Artis Wajib Diisi.",
          minlength: "Minimal 2 character."
        },               
        bhsa: {
          required: "Bahasa Lagu Wajib Diisi.",
          minlength: "Minimal 2 character."
        },               
        genres: {
          required: "Wajib Pilih Salah Satu Genre.",
        },               
        song_lyric: {
          required: "Lirik Lagu Wajib Diisi.",
          minlength: "Minimal 2 character."
        },               
        cp: {
          required: "Kontak Wajib Diisi.",
          minlength: "Minimal 2 character."
        },               
        image: {
          required: "Gambar Cover Wajib Diisi.",
          minlength: "Minimal 2 character."
        },               
        penulis: {
          required: "Penulis Wajib Diisi.",
          minlength: "Minimal 2 character."
        },               
        komposer: {
          required: "Komposer Wajib Diisi.",
          minlength: "Minimal 2 character."
        },               
        thn_prdksi: {
          required: "Tahun Produksi Wajib Diisi.",
          minlength: "Harus 4 character."
        },               
        store: {
          required: "Store Wajib Dipilih",
        },
        agree: "Harap Centang Jika Setuju"
      },
      submitHandler: function(form) {

        loading();

        var uri = '<?=$site_url?>proses/discography/singlesave.php';
        var formData = new FormData($('#submitForm')[0]);

        $.ajax({
          type: "POST",
          url: uri,
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success: function(response){
            console.log(response);
            if( ! response.status){                 
              Swal.fire({
                type: 'warning',
                text: response.message,
              })
            } else{
              Swal.fire({
                type: 'success',
                text: response.message,
              })
              setTimeout(function(){
                window.location.replace('<?=$site_url?>index.php?single');
              }, 1000);
            }
          },
          error: function(err){
            console.log(err);
            Swal.fire({
              type: 'error',
              text: err.status + " : " + err.statusText,
            })
          }
        });

        return false;
      }
    });
  </script>

  <script type="text/javascript">

// $('#tes').on('click', function(){
//   loading();
// })
</script>