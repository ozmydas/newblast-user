<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">Publisher</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="index.php?dashboard">Home</a></li>
        <li>Dashboard</li>
        <li>Add New Album</li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Lihat Album Kamu

          <a href="<?=$site_url?>index.php?album" style="float: right; padding-top: 4px; padding-bottom: 4px; margin-top: 3px; font-weight: bold;" class="btn btn-sm btn-primary"><span class="fa fa-chevron-left"></span> Kembali</a>
        </header>
        <div class="panel-body">
          <!--  -->

          <div class="row">
            <div class="col-lg-12">

              <!--  -->
              <div class="form">
                <form class="form-validate form-horizontal " id="submitForm"  action="" method="post" enctype="multipart/form-data">

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Judul Album <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <input class=" form-control" id="an" name="an" type="text" required />
                    </div>
                  </div>

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Artist <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <input class=" form-control" id="artist" name="artist" type="text" required />
                    </div>
                  </div>

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Genre <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <select class="form-control" type="text" name="genres" id="genres" required>
                       <option value="" selected disabled>-- Select Genre --</option>
                       <?php 
                       foreach($list_genre as $g){
                         ?>
                         <option value="<?php echo $g['id']; ?>"><?php echo $g['name']; ?></option>
                         <?php
                       }
                       ?>
                     </select>

                     <small>Jenis aliran musik yang utama</small>
                   </div>
                 </div>

                 <div class="form-group">
                  <label class="control-label col-lg-2">Tanggal Rilis <span class="required">*</span></label>
                  <?php
                  date_default_timezone_set("Asia/Jakarta");
                  $date_now = date("Y-m-d");
                  $date_plus_14 = date("Y-m-d", strtotime('+14 days', strtotime($date_now)));
                  ?>

                  <div class="col-lg-9 col-md-12">
                    <input class="form-control" type="date" name="tgl_release" min="<?php echo $date_plus_14; ?>" value="<?php echo $date_plus_14; ?>" />
                    <small>Tanggal mulainya rilis penjualan lagu (Minimal 14 hari setelah single ditambahkan)</small>
                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Deskripsi Album </label>
                  <div class="col-lg-9 col-md-12">
                    <textarea class=" form-control" id="deskripsi" name="da" type="text"></textarea>
                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Contact Person <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control" id="phone" name="phone" type="text" required />
                  </div>
                </div>


                 <div class="form-group ">
                  <label class="control-label col-lg-2">Label</label>
                  <div class="col-lg-9 col-md-12">

                    <input type="radio" class="lbl" name="label" value="mblast" checked> Musicblast.id --> Rp. 100.000<br>
                     <input type="radio" class="lbl" name="label" value="custom"> Custom Label --> Rp. 200.000<br>
                    <input class="form-control" type="text" class="lblnm" name="lebelname"/>

                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Pencipta Lagu  <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control" id="penulis" name="pnlis" type="text" required />
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Komposer  <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control" id="kmposer" name="kmposer" type="text" required />
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Tahun Produksi  <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control" id="thn_prdksi" name="thn_prodksi" type="text" required />
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Cover <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class="dropify" data-height="150" id="image" name="image" type="file" required />
                    <small>Mohon masukkan gambar dengan resolusi 1440x1440 px , High Resolusion/TIdak Blur</small>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Songs <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class="form-control" type="file" name="song[]" accept="audio/x-wav" multiple required />
                    <small>Untuk memilih beberapa lagu sekaligus, klik file sambil menekan tombol CTRL. Mohon Masukan Lagu Dengan Format WAV</small>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">KTP <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class="dropify" data-height="150" id="ktp" name="ktp" type="file" accept="image/jpeg, image/png" required />
                    <small>Mohon Masukkan KTP Penanggung Jawab (format JPG/PNG)</small>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Store <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input type="checkbox" name="store[]" value="All Store (7-14 hari kerja)" checked> All Store (7-14 hari kerja)<br>
                    <input type="checkbox" name="store[]" value="Joox (45 hari Kerja)" > Joox (45 hari Kerja)<br>
                    <input type="checkbox" name="store[]" value="Youtube Service/Fingerprint" > Youtube Service/Fingerprint<br>
                    <input type="checkbox" name="store[]" value="Local Store" > Local Store (RBT, Melon Musik, Langit Musik. 45 hari Kerja)  Berbayar Rp. 350.000<br>
                  </div>
                </div>



                <div class="form-group ">
                  <label for="agree" class="control-label col-lg-2 col-sm-3"> <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12 col-sm-9">
                    Setuju dan Telah Membaca <a href="index.php?sk" target="_blank">Syarat dan Ketentuan</a>
                    <input type="checkbox" style="width: 20px" class="checkbox form-control" id="agree" name="agree" />
                    <br>
                    <span class="required">*</span> Pastikan mengisi dengan benar, data yang telah dikonfirmasi tidak dapat diubah. 
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-9 col-md-12">
                    <button class="btn btn-primary" type="submit" name="submit">Save</button>
                  </div>
                </div>
              </form>
            </div>
            <!--  -->

          </div>
        </div>

        <!--  -->
      </div>
    </section>
  </div>
</div>

<!-- page end-->
</section>

<?php
include("album-js-add.php");
?>

<style type="text/css">

</style>