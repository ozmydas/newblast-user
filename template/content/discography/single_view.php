<?php
$_SESSION['INPUT_FORM'] = [
  'input_id' => base64_encode(sha1(date('Ymd').mt_rand()))
];
?>

<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">Informasi single</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="index.php?dashboard">Home</a></li>
        <li><a href="<?=$site_url?>index.php?single">Single</a></li>
        <li>Detail</li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Detail Informasi Single Kamu

          <a href="<?=$site_url?>index.php?single" style="float: right; padding-top: 4px; padding-bottom: 4px; margin-top: 3px; font-weight: bold;" class="btn btn-sm btn-primary"><span class="fa fa-chevron-left"></span> Kembali</a>
        </header>
        <div class="panel-body">
          <!--  -->

          <div class="row">
            <div class="col-lg-12">
              <div class="alert alert-warning text-center hide-edit" style="color: orange; display: none; ">
                Silahkan Edit Informasi Single.<br/>Untuk Cover dan File Lagu, Tidak Bisa Diubah.
              </div>

              <!--  -->
              <div class="form">
                <form class="form-validate form-horizontal " id="submitForm"  action="" method="post" enctype="multipart/form-data">

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Judul Lagu <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <input type="hidden" name="<?=$_SESSION['INPUT_FORM']['input_id']?>" value="<?=$_GET['id']?>">
                      <input class=" form-control editable" id="title" name="title" type="text" value="<?=$single["title"]?>" disabled required />
                    </div>
                  </div>

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Artist <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <input class=" form-control editable" id="artist" name="artist" type="text"  value="<?=$single["artist"]?>" disabled required  />
                    </div>
                  </div>

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Feat Artist </label>
                    <div class="col-lg-9 col-md-12">
                      <input class=" form-control editable" id="ft_artist" name="ft_artist" type="text" value="<?=$single["ft_artist"]?>" disabled />
                    </div>
                  </div>

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Bahasa <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <input class=" form-control editable" id="bhsa" name="bhsa" type="text"  value="<?=$single["bhsa"]?>" disabled required />
                    </div>
                  </div>

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Genre <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <select class="form-control" type="text" name="genres" id="genres" readonly>
                       <option value="" selected disabled>-- Select Genre --</option>
                       <?php 
                       foreach($list_genre as $g){
                         ?>
                         <option value="<?php echo $g['id']; ?>" <?=$g['id']==$single['genre_id']?'selected':'';?> ><?php echo $g['name']; ?></option>
                         <?php
                       }
                       ?>
                     </select>

                     <small>Jenis aliran musik yang utama</small>
                   </div>
                 </div>

                 <div class="form-group">
                  <label class="control-label col-lg-2">Tanggal Rilis <span class="required">*</span></label>
                  <?php
                  date_default_timezone_set("Asia/Jakarta");
                  $date_now = date("Y-m-d");
                  $date_plus_14 = date("Y-m-d", strtotime('+14 days', strtotime($date_now)));
                  ?>

                  <div class="col-lg-9 col-md-12">
                    <input class="form-control" type="date" name="tgl_release" value="<?php echo $single['start_date']; ?>" readonly />
                    <small>Tanggal mulainya rilis penjualan lagu (Minimal 14 hari setelah single ditambahkan)</small>
                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Press Release </label>
                  <div class="col-lg-9 col-md-12">
                    <textarea class=" form-control editable" id="press_release" name="press_release" type="text" disabled><?=$single["press_release"]?></textarea>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Lirik Lagu <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <textarea class=" form-control editable" id="song_lyric" name="song_lyric" type="text" disabled required><?=$single["lirik"]?></textarea>
                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Contact Person <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control editable" id="cp" name="cp" type="text"  value="<?=$single["cp"]?>" disabled required/>
                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Akun Instagram</label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control editable" id="ig" name="ig" type="text" value="<?=$single["ig"]?>" disabled />
                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Akun Facebook</label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control editable" id="fb" name="fb" type="text" value="<?=$single["fb"]?>" disabled />
                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Label</label>
                  <div class="col-lg-9 col-md-12">
                    <!-- <input type="checkbox" class="lbl" name="label" value="" <?= $value['label']!='musicblast.id'?'checked':'' ?>> Custom Label (Jika memakai nama label sendiri berbayar Rp. 200.000)<br> -->
                    <input class="form-control" type="text" class="lblnm" name="lebelname" value="<?=$single["label"]?>" disabled/>
                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Pencipta Lagu  <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control editable" id="penulis" name="penulis" type="text"  value="<?=$single["penulis"]?>" disabled required />
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Komposer  <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control editable" id="komposer" name="komposer" type="text"  value="<?=$single["komposer"]?>" disabled required />
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Tahun Produksi  <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <input class=" form-control editable" id="thn_prdksi" name="thn_prdksi" type="text"  value="<?=$single["thn_prdksi"]?>" disabled required />
                  </div>
                </div>


                <div class="form-group file-view">
                  <label class="control-label col-lg-2">Cover <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <!-- <input class="dropify" data-height="150" id="image" name="image" type="file" required /> -->
                    <!-- <small>Mohon masukkan gambar dengan resolusi 1440x1440 px , High Resolusion/TIdak Blur</small> -->
                    <img  class="img" style="height: 150px; width: auto;" src="https://drive.google.com/uc?export=view&id=<?=$single['image']?>">
                  </div>
                </div>

                <div class="form-group  file-view">
                  <label class="control-label col-lg-2">Song <span class="required">*</span></label>
                  <div class="col-lg-9 col-md-12">
                    <audio controls>
                      <source src="https://drive.google.com/uc?export=view&id=<?=$single['file']?>" type="audio/mpeg">
                        Your browser does not support the audio element.
                      </audio>
                    </div>
                  </div>

                  <div class="form-group  file-view">
                    <label class="control-label col-lg-2">KTP <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <!-- <input class="dropify" data-height="150" id="ktp" name="ktp" type="file" accept="image/jpeg, image/png" required />
                        <small>Mohon Masukkan KTP Penanggung Jawab (format JPG/PNG)</small> -->
                        <img  class="img" style="height: 150px; width: auto;" src="https://drive.google.com/uc?export=view&id=<?=$single['ktp']?>">
                      </div>
                    </div>


                    <div class="form-group  file-edit">
                      <label class="control-label col-lg-2">Cover</label>
                      <div class="col-lg-9 col-md-12">
                        <input class="dropify" data-height="150" id="image" name="image" type="file" data-default-file="https://drive.google.com/uc?export=view&id=<?=$single['image']?>"/>
                        <small>Mohon masukkan gambar dengan resolusi 1440x1440 px , High Resolusion/TIdak Blur</small>
                      </div>
                    </div>

                    <div class="form-group  file-edit">
                      <label class="control-label col-lg-2">Song</label>
                      <div class="col-lg-9 col-md-12">
                        <input class="form-control" type="file" name="song" accept="audio/x-wav"/>
                        <small>Masukan Lagu Dengan Format WAV Untuk Mengubah Lagu. Biarkan kosong jika tetap memakai lagu lama</small>
                      </div>
                    </div>

                    <div class="form-group  file-edit">
                      <label class="control-label col-lg-2">KTP</label>
                      <div class="col-lg-9 col-md-12">
                        <input class="dropify" data-height="150" id="ktp" name="ktp" type="file" accept="image/jpeg, image/png" data-default-file="https://drive.google.com/uc?export=view&id=<?=$single['ktp']?>"/>
                        <small>Mohon Masukkan KTP Penanggung Jawab (format JPG/PNG)</small>
                      </div>
                    </div>


                  <!-- <div class="form-group ">
                    <label class="control-label col-lg-2">Store <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">
                      <input type="checkbox" name="store[]" value="All Store (7-14 hari kerja)" checked> All Store (7-14 hari kerja)<br>
                      <input type="checkbox" name="store[]" value="Joox (45 hari Kerja)" > Joox (45 hari Kerja)<br>
                      <input type="checkbox" name="store[]" value="Youtube Service/Fingerprint" > Youtube Service/Fingerprint<br>
                      <input type="checkbox" name="store[]" value="Local Store" > Local Store (RBT, Melon Musik, Langit Musik. 45 hari Kerja)  Berbayar Rp. 350.000<br>
                    </div>
                  </div> -->

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Store  <span class="required">*</span></label>
                    <div class="col-lg-9 col-md-12">

                      <?php
                      $stores = explode(',', $single['store']);
                      foreach ($stores as $key => $value): ?>
                        <?=$value?>
                      <?php endforeach; ?>
                      
                    </div>
                  </div>

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Status</label>
                    <div class="col-lg-9 col-md-12">
                      <input class=" form-control" id="status" name="status" type="text"  value="<?=$single["status"]?>" disabled  />
                    </div>
                  </div>

                  <div class="form-group ">
                    <label class="control-label col-lg-2">Keterangan</label>
                    <div class="col-lg-9 col-md-12">
                      <textarea class=" form-control" id="keterangan" name="keterangan" disabled><?=$single["Keterangan"]?></textarea>
                    </div>
                  </div>


                  <div class="form-group ">
                    <label class="control-label col-lg-2">Total Harga (Rp)</label>
                    <div class="col-lg-9 col-md-12">
                      <input class=" form-control" id="total" name="total" type="text"  value="<?=number_format($single["price"], 0, '', '.')?>" disabled required />
                      <small>Kami Akan Proses Jika Pembayaran Telah Di Terima Dengan Mengupload Bukti Transfer</small>
                    </div>
                  </div>


                  <?php if($single['status'] == "Belum Terkonfirmasi"): ?>

                    <?php if(empty($single['bukti'])):?>

                      <div class="form-group">
                        <label class="control-label col-lg-2">Upload Bukti Transfer <span class="required">*</span></label>
                        <div class="col-lg-9 col-md-12">
                          <input class="dropify" data-height="150" id="bukti" name="bukti" type="file" accept="image/jpeg, image/png" />
<br/>
                          <span class="btn btn-primary" id="btnBayar">UPLOAD BUKTI TRANSFER</span><br/>
                          <small>Mohon Upload Bukti Transfer Agar Dapat Diproses Lebih Lanjut</small>
                        </div>
                      </div>

                      <?php else: ?>

                        <div class="form-group">
                          <label class="control-label col-lg-2">Bukti Transfer</label>
                          <div class="col-lg-9 col-md-12">
                            <!-- <input class="dropify" data-height="150" id="image" name="image" type="file" required /> -->
                            <!-- <small>Mohon masukkan gambar dengan resolusi 1440x1440 px , High Resolusion/TIdak Blur</small> -->
                            <img  class="img" style="height: 150px; width: auto;" src="https://drive.google.com/uc?export=view&id=<?=$single['bukti']?>">
                            <small></small>
                          </div>
                        </div>

                      <?php endif; ?>



                    <?php endif; ?>


                  <!-- <div class="form-group ">
                    <label for="agree" class="control-label col-lg-2 col-sm-3"></label>
                    <div class="col-lg-9 col-md-12 col-sm-9">
                      <span class="required">*</span> Setuju dan Telah Membaca <a href="index.php?sk" target="_blank">Syarat dan Ketentuan</a>
                      <input type="checkbox" style="width: 20px" class="checkbox form-control" id="agree" name="agree" />
                      <br>
                      <span class="required">*</span> Pastikan mengisi dengan benar, data yang telah dikonfirmasi tidak dapat diubah.
                    </div>
                  </div> -->

                </form>

                <hr>

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-9 col-md-12">
                    <!-- <button class="btn btn-primary" type="submit" name="submit">Save</button> -->
                    <a href="<?=$site_url?>index.php?single" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali ke Single</a>

                    <?php if($single['status'] == "Belum Terkonfirmasi"): ?>
                      <span class="btn btn-success" id="btnEdit"><i class="fa fa-edit"></i> Edit single</span>
                      <span class="btn btn-warning" id="btnUpdate" style="display: none;"><i class="fa fa-edit"></i> Update single</span>
                    <?php endif; ?>

                  </div>
                </div>
              </div>
              <!--  -->

            </div>
          </div>

          <!--  -->
        </div>
      </section>
    </div>
  </div>

  <!-- page end-->
</section>

<?php  include('single-js-edit.php'); ?>