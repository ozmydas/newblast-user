<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">My Single</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="#">Home</a></li>
        <li>Discography</li>
        <li>Single</li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Lihat Single Kamu

          <a href="<?=$site_url?>index.php?add_single" style="float: right; padding-top: 4px; padding-bottom: 4px; margin-top: 3px; font-weight: bold;" class="btn btn-sm btn-primary"><span class="fa fa-edit"></span> Upload Single Baru</a>
        </header>
        <div class="panel-body">
          <!--  -->

          <!-- DISINI TABLE NYA -->
          <table id="myTable" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th class="text-center">Judul Lagu</th>
                <th class="text-center">Artis</th>
                <th class="text-center">Cover</th>
                <th class="text-center">Tgl Upload</th>
                <th class="text-center">Status</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          <!-- SAMPAI SINI -->

          <!--  -->
        </div>
      </section>
    </div>
  </div>

  <!-- page end-->
</section>

<script type="text/javascript">
  $(document).ready(function() {
    table = $('#myTable').DataTable( {
      "processing": true,
      "serverSide": true,
      "ajax": {
           "url" : "<?=$site_url?>proses/discography/singlelist.php",
          "type": "POST",
      },
      'lengthChange': true,
      "order": [],
      'autoWidth'   : false,
      'scrollX'  : true,
      "columnDefs": [
        { 
          "targets": [2,3,4,5],
          "class": "text-center width-legacy",
        },
        { 
          "targets": [2, 5],
          "orderable": false,
        },
      ], 
      });
  });
</script>

<style type="text/css">
  .width-legacy img{
    max-width: 100px;
  }
</style>