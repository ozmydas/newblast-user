<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">My Profile</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
        <li>Profile</li>
        <li>Overview</li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Informasi Profile

          <button id="toogleTab" style="float: right; padding-top: 4px; padding-bottom: 4px; margin-top: 3px; font-weight: bold;" class="btn btn-sm btn-primary" ><span class="fa fa-edit"></span> <span id="textBtn">Ubah Profile</span></button>
        </header>
        <!--  -->

        <div class="panel-body bio-graph-info" id="tab1">
          <div class="bio-graph-heading" style="padding-top: 10px; padding-bottom: 10px; background: #f0f0f0; background-size: cover; margin-bottom: 20px">
            <img src="<?=$site_url?>upload/profil/<?=$foto?>" class="img-rounded" style="height: 120px; width: auto;">
          </div>

          <div class="row">

            <?php foreach($profiles as $col): ?>
              <div class="col-lg-6">

                <?php foreach($col as $key => $value): ?>
                  <div class="bios-row">
                    <div class="bios-l">
                      <label><?=$key?></label>
                    </div>
                    <div class="bios-m">:</div>
                    <div class="bios-r">
                      <label><?=$value?$value:"-"?></label>
                    </div>
                  </div>
                <?php endforeach; ?>

              </div>
            <?php endforeach; ?>

          </div>
        </div>




        <div class="panel-body bio-graph-info" id="tab2" style="display: none; margin-top: 0px">
          <form class="form-validate form-horizontal " id="myForm" method="POST">

            <div class="bio-graph-heading" style="padding-top: 10px; padding-bottom: 10px; background: #f0f0f0; background-size: cover; margin-bottom: 20px; display: flex; align-items: center; justify-content: center;">

              <input type="file" name="foto" class="dropify" data-height="120" data-default-file="<?=$site_url?>upload/profil/<?=$foto?>">
            </div>

            <div class="row">

              <div class="col-lg-6 longgar">

                <div class="row" style="margin-bottom: 10px">
                  <div class="form-group">
                    <label for="fullname" class="control-label col-lg-3 col-xs-12 col-sm-12 col-md-3">Nama Lengkap <span class="required">*</span></label>
                    <div class="col-lg-8 col-xs-12 col-sm-12 col-md-8">
                      <input class=" form-control" id="fullname" name="fullname" value="<?=@$profiles[0]['Nama Lengkap']?>" type="text" required/>
                    </div>
                  </div>
                </div>

                <div class="row" style="margin-bottom: 10px">
                  <div class="form-group">
                    <label for="fullname" class="control-label col-lg-3 col-xs-12 col-sm-12 col-md-3">Email <span class="required">*</span></label>
                    <div class="col-lg-8 col-xs-12 col-sm-12 col-md-8">
                      <input class=" form-control" id="email" name="email" value="<?=@$profiles[0]['Email']?>" type="text" readonly/>
                    </div>
                  </div>
                </div>

                <div class="row" style="margin-bottom: 10px">
                  <div class="form-group">
                    <label for="fullname" class="control-label col-lg-3 col-xs-12 col-sm-12 col-md-3">Handphone <span class="required">*</span></label>
                    <div class="col-lg-8 col-xs-12 col-sm-12 col-md-8">
                      <input class=" form-control" id="hp" name="hp" value="<?=@$profiles[0]['No. Handphone']?>" type="text" required/>
                    </div>
                  </div>
                </div>

              </div>

              <div class="col-lg-6 longgar">

                <div class="row" style="margin-bottom: 10px">
                  <div class="form-group">
                    <label for="fullname" class="control-label col-lg-3 col-xs-12 col-sm-12 col-md-3">Nama Bank <span class="required">*</span></label>
                    <div class="col-lg-8 col-xs-12 col-sm-12 col-md-8">
                      <input class=" form-control" id="bankname" name="bankname" value="<?=@$profiles[1]['Nama Bank']?>" type="text" required/>
                    </div>
                  </div>
                </div>

                <div class="row" style="margin-bottom: 10px">
                  <div class="form-group">
                    <label for="fullname" class="control-label col-lg-3 col-xs-12 col-sm-12 col-md-3">Nomor Rekening <span class="required">*</span></label>
                    <div class="col-lg-8 col-xs-12 col-sm-12 col-md-8">
                      <input class=" form-control" id="bankrek" name="bankrek" value="<?=@$profiles[1]['No. Rekening']?>" type="text" required/>
                    </div>
                  </div>
                </div>

                <div class="row" style="margin-bottom: 10px">
                  <div class="form-group text-right">
                    <div class="col-lg-11 col-md-11">
                      <button class="btn btn-success">SIMPAN</button>
                    </div>
                  </div>
                </div>

              </div>

            </div>

          </form>
        </div>

        <!--  -->
      </section>
    </div>
  </div>

  <!-- page end-->
</section>

<script type="text/javascript">
  $('.dropify').dropify();

  var satu = true;
  $('#toogleTab').on("click", function(){
    if(satu){
      $('#tab1').css("display", "none");
      $('#tab2').fadeIn('slow');
      $('#textBtn').text("Kembali");
    } else {
      $('#tab2').css("display", "none");
      $('#tab1').fadeIn('slow');
      $('#textBtn').text("Ubah Profile");
    }

    satu = ! satu;
  })


  $('#myForm').on('submit', function(e){
    e.preventDefault();

    loading();

    var uri = '<?=$site_url?>proses/user/profile-edit.php';
    var formData = new FormData($('#myForm')[0]);

    $.ajax({
      type: "POST",
      url: uri,
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response){
        console.log(response);
        if( ! response.status){                 
          Swal.fire({
            type: 'warning',
            text: response.message,
          })
        } else{
          Swal.fire({
            type: 'success',
            text: response.message,
          })
          setTimeout(function(){
            window.location.replace('<?=$site_url?>index.php?profil');
          }, 700);
        }
      }, 
      error: function(err){
        console.log(err);
        Swal.fire({
          type: 'error',
          text: err.status + " : " + err.statusText,
        })
      }
    });
  })

</script>

<style type="text/css">
  .bio-graph-heading .dropify-wrapper{
    width: 240px !important;
    border-radius: 5px;
  }
  .bio-graph-heading .dropify-render img{
    height: 120px !important;
    width: auto;
  }

  .bios-row{
    display: flex;
    flex-direction: row;
    padding-bottom: 15px;
  }

  .bios-l{
    width: 150px;
  }

  .bios-m{
    width: 20px;
  }

  .bios-r{
    flex: 1;
  }

  .dropify-filename-inner{
    display: none !important;
  }


  @media only screen and (max-width: 990px) {
    .longgar{
      margin-left: 15px;
      margin-right: 15px;
    }

    .longgar label{
      text-align: left !important;
    }
  }
</style>