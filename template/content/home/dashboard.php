<section class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">Dashboard User</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
        <li>Dashboard</li>
        <li>Overview</li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  

  <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <div class="info-box animated fadeInUp duration-2s blue-bg">
		<i class="fa fa-music"></i>
        <div class="count"><?=$total_single?></div>
        <div class="title">Total Single</div>
      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <div class="info-box animated fadeInUp duration-2s brown-bg animate-order-1">
        <i class="fa fa-headphones"></i>
        <div class="count"><?=$total_album?></div>
        <div class="title">Total Album</div>
      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <div class="info-box animated fadeInUp duration-2s dark-bg animate-order-2">
        <i class="fa fa-shopping-cart"></i>
        <div class="count">Cart</div>
        <!-- <div class="title">Store</div> -->
      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <div class="info-box animated fadeInUp duration-2s green-bg animate-order-3">
        <i class="fa fa-cubes" style="margin-right: 0 !important"></i>
        <div class="count">Service</div>
        <!-- <div class="title">Service</div> -->
      </div>
    </div>

  </div>

  <div class="row">
    <h1 class="text-center">Your Releases</h1>
  </div>

  <div class="row animate-order-3">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Latest Single Release
        </header>
        <div class="panel-body">
          <div class="discography scroller" style="">

            <?php $i=0;
            foreach($latest_single as $key => $value): ?>
              <div class="discography-item" style="">
                <div class="discography-img" style="">
                  <img src="https://drive.google.com/uc?export=view&id=<?=$value['image']?>" style="height: 100%">
                </div>
                <div  class="discography-title">
                  <?=strlen($value["title"])>32 ? substr($value["title"], 0, 32)."..." : $value["title"];?>
                  <?php if(empty($value["title"])){echo'- Tidak Ada Judul -';}?>
                </div>
              </div>
            <?php endforeach; ?>

            
            <div class="discography-item">
              <div class="discography-plus" style="">
                <a href="<?=$site_url?>index.php?add_single"><span style="display: inline-block; font-size: 8em; color: #fff; line-height: 150px;">+</span></a>
              </div>
              <div class="text-center" style="padding: 10px 0 20px 0">Tambah Single Baru</div>
            </div>

          </div> 

        </div>
      </section>
    </div>
  </div>

  <div class="row animate-order-4">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Latest Album Release
        </header>
        <div class="panel-body">

          <div class="discography scroller" style="">

            <?php $i=0;
            foreach($latest_album as $key => $value): ?>
              <div class="discography-item" style="">
                <div class="discography-img" style="">
                  <img src="https://drive.google.com/uc?export=view&id=<?=$value['cover_album']?>" style="height: 100%">
                </div>
                <div  class="discography-title">
                  <?=strlen($value["album_name"])>32 ? substr($value["album_name"], 0, 32)."..." : $value["album_name"];?>
                  <?php if(empty($value["album_name"])){echo'- Tidak Ada Judul -';}?>
                </div>
              </div>
            <?php endforeach; ?>

            
            <div class="discography-item">
              <div class="discography-plus" style="">
                <a href="<?=$site_url?>index.php?add_album"><span style="display: inline-block; font-size: 8em; color: #fff; line-height: 150px;">+</span></a>
              </div>
              <div class="text-center" style="padding: 10px 0 20px 0">Tambah Album Baru</div>
            </div>

          </div> 

        </div>
      </section>
    </div>
  </div>

  <!-- page end-->
</section>


<style type="text/css">




  .info-box {
    border-radius: 30px 0 30px 0;
    box-shadow: 0 0 0 #666;
    transition: all 0.7s;
    cursor: pointer;
  }

  .info-box:hover {
    transform: scale(1.1);
    box-shadow: 1px 1px 2px #666, 1px 1px 5px #ddd;
  }


  .discography.scroller{
    overflow-x: auto;
    white-space: nowrap;
  }

  .discography-item{
    width: 200px;
    display: inline-block;
    white-space: nowrap;
    margin-left:10px;
    text-align: center;
  }

  .discography-img{
    height: 150px;
    width: 200px;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 5px;
    border: 1px solid #eaeaea;
  }

  .discography-title{
    padding: 10px 0 20px 0;
  }

  .discography-plus{
    height: 150px;
    width: 150px;
    background: #FEB466;
    border-radius: 50%;
    display: inline-block;
    text-align: center;
    cursor: pointer;
  }

</style>


<script type="text/javascript">

  $('.scroller').niceScroll({
    cursorwidth:"14px"
  });

</script>