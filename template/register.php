<?php 
session_start();
$_SESSION['INPUT_FORM'] = [
    "email" => md5(mt_rand()),
    "name" => md5(mt_rand()),
    "password" => md5(mt_rand()),
    "re_password" => md5(mt_rand()),
    "no_hp" => md5(mt_rand()),
];

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Halaman Register</title>
    <link href="<?=$site_url?>assets/css/bootstrap.css" rel="stylesheet">
    <!-- <link href="css/sb-admin.css" rel="stylesheet" /> -->
    <link href="images/mb.ico" type="image/x-icon" rel="Shortcut Icon" />
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit&hl=id" async defer></script>

<style>
    body, html{
        background-image: url('<?=$site_url?>assets/img/bg.jpg');
        background-size: cover;
        position: center;
    }
</style>

</head>
<body>
    <div style="width: 100%; height: 80px;"></div>
    <div style="width: 100%; height: 80px;"></div>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pendaftaran Member</h3>
                    </div>
                    <div class="panel-body">
                        <form id="myForm">
                            <fieldset>

                                <div class="form-group">
                                    <input class="form-control" placeholder="Nama Lengkap" name="<?=$_SESSION['INPUT_FORM']["name"]?>" type="text" autofocus="autofocus" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="No. HP" name="<?=$_SESSION['INPUT_FORM']["no_hp"]?>" type="phone" autofocus="autofocus" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email" name="<?=$_SESSION['INPUT_FORM']["email"]?>" type="email" autofocus="autofocus" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="<?=$_SESSION['INPUT_FORM']["password"]?>" type="password" value="" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Ulangi Password" name="<?=$_SESSION['INPUT_FORM']["re_password"]?>" type="password" value="" required />
                                </div>


                                <div class="form-group">
                                    <div class="alert alert-warning text-center captcha-text">
                                        loading captcha
                                    </div>
                                    <div id="recaptcha">
                                    </div>
                                </div>

                                <input type="submit" name="submit_login" value="Register" class="btn btn-lg btn-success btn-block disabled"/>
                                <br>
                                <div>
                                    <a href="login.php"><i class="fas fa-user"></i> Login?</a>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="../user/assets/js/jquery.js"></script>
    <script src="../user/assets/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/7663bd514c.js"></script>
    <script src="<?=$site_url?>assets/js/fusion.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script>
        var captchaExpired = function(){
            grecaptcha.reset();
            $('[name="submit_login"]').addClass("disabled");
        }

        var captchaNextStage = function(response){
            $('[name="submit_login"]').removeClass("disabled");
        }

        var onloadCallback = function() {
            grecaptcha.render("recaptcha",{
                'sitekey': "6LcDpK4UAAAAAJBXd_3L2vV9L_a8qeCdURVH0BDq",
                'callback': captchaNextStage,
                'expired-callback': captchaExpired
            })

            $('.captcha-text').fadeOut();
        };


        $('#myForm').on("submit", function(e){
            e.preventDefault();
            loading();
            prosesForm();
        });

        function prosesForm(){
        // get form data
        var formData = new FormData($("#myForm")[0]);
        var uri = "proses/user/user-register.php";

        $.ajax({
            type: "POST",
            url: uri,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(response){

                if( ! response.status){
                    Swal.fire({
                        type: 'warning',
                        text: response.message,
                    })

                    captchaExpired();
                    return false;
                }

                Swal.fire({
                    type: 'success',
                    text: response.message,
                })
                setTimeout(function(){
                    window.location.replace(response.data);
                }, 500);
            },
            error: function(err){
                Swal.fire({
                    type: 'error',
                    text: err.status + " : " + err.statusText,
                })

                captchaExpired();
            }
        });
    }

    
</script>

</body>
</html>