<header class="header dark-bg">
  <div class="toggle-nav">
    <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
  </div>

  <!--logo start-->
  <a href="index.php?dashboard" class="logo">Music<span class="lite">blast</span></a>
  <!--logo end-->

  <div class="nav search-row" id="top_menu">
    <!--  search form start -->
    <ul class="nav top-menu">
      <li>
        <form class="navbar-form">
          <!-- <input class="form-control" placeholder="Cari Single Kamu. . ." type="text"> -->
        </form>
      </li>
    </ul>
    <!--  search form end -->
  </div>

  <div class="top-nav notification-row">
    <!-- notificatoin dropdown start-->
    <ul class="nav pull-right top-menu">

      <!-- task notificatoin start -->


      <!-- task notificatoin end -->
      <!-- inbox notificatoin start-->

      <!-- inbox notificatoin end -->
      <!-- alert notification start-->
      <li id="alert_notificatoin_bar" class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">

          <i class="icon-envelope-l"></i>
        <?php if(count($mymessages)): ?>
          <span class="badge bg-important"><?=count($mymessages)?></span>
        <?php endif; ?>
        </a>
        <ul class="dropdown-menu extended notification">
          <div class="notify-arrow notify-arrow-blue"></div>
          <li>
            <a href="<?=$site_url?>index.php?message" style="font-weight: bold;">
              <i class="fa fa-chevron-left" style="margin-right: 5px;"></i> Lihat Semua Pesan Masuk
            </a>
          </li>
          
          <?php if(count($mymessages)): ?>
            <li>
              <p class="blue"><?=count($mymessages)?> pesan baru</p>
            </li>
          <?php else: ?>
            <li>
              <p class="blue">Tidak Ada pesan baru</p>
            </li>
          <?php endif; ?>
          
      <?php $mi = 1;
        foreach($mymessages as $key => $value): ?>
        <li>
          <a href="<?=$site_url.'index.php?message='.$value['id_pesan']?>">
            <i class="fa fa-envelope" style="margin-right: 5px;"></i> 
            <?=$value['subyek_pesan']?>
            <!-- <span class="small italic pull-right">5 mins</span> -->
          </a>
        </li>
      <?php 
      if($mi++ >= 5):
        break;
      endif;
      endforeach;?>

    </ul>

    <li id="alert_notificatoin_bar" class="dropdown">
      <a data-toggle="dropdown" class="dropdown-toggle" href="#">

        <i class="icon-bell-l"></i>
        <span class="badge bg-important">1</span>
      </a>
      <ul class="dropdown-menu extended notification">
        <div class="notify-arrow notify-arrow-blue"></div>
        <li>
          <p class="blue">You have 1 new notifications</p>
        </li>
        <li>
          <a href="#">
            <span class="label label-primary"><i class="icon_profile"></i></span>
            Friend Request
            <span class="small italic pull-right">5 mins</span>
          </a>
        </li>
              <!-- <li>
                <a href="#">See all notifications</a>
              </li> -->
            </ul>
          </li>
          <!-- alert notification end--> 

          <!-- user login dropdown start-->
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <span class="profile-ava">
                <img alt="" src="../user/assets/img/avatar1_small.jpg">
              </span>
              <span class="username"><?=$userdata["name"]?></span>
              <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
              <div class="log-arrow-up"></div>
              <li class="eborder-top">
                <a href="<?=$site_url?>index.php?profil"><i class="icon_profile"></i>PROFILE SAYA</a>
              </li>
              <li class="eborder-top">
                <a href="<?=$site_url?>login.php"><i class="icon_key_alt"></i>LOGOUT</a>
              </li>
            </ul>
          </li>
          <!-- user login dropdown end -->

        </ul>

        <!-- notificatoin dropdown end-->
      </div>
    </header>