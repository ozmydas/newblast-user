<html>
<head>
	<title>{{mail_subject}}</title>
</head>
<body style="background: #f7f7f7">

	<div style="width: 100%; background: #f7f7f7; margin: 0; padding: 0">
		<div style="max-width: 680px; background: #fff; margin-left: auto; margin-right: auto; margin-top: 50px; margin-bottom: 50px; border: 1px solid #ddd; border-radius: 10px; font-family: Arial, Helvetica, sans-serif; padding: 0px;">

			<div style="background: url('http://t-hisyam.net/assets/img/mblast.png') no-repeat, #000; background-size: contain; height: 120px; border-radius: 10px 10px 0 0">

			</div>

			<h2 style="text-align: center; font-weight: normal;">{{mail_subject}}</h2>

			<p style="padding: 0 20px;">{{mail_content}}</p>

			<div style="border-bottom: 1px solid #ddd; margin-top: 40px; margin-bottom: 30px;"></div>

			<div style="text-align: center; color: #aaa">
				<div style="text-align: left; color: #888; padding: 0 20px;">
					Contact : 
				</div>
				<p><?=date("Y")?> &copy; MusicBlast.id All Right Reserved.</p>
			</div>
		</div>
	</div>

</body>
</html>
