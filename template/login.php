<?php 
$_SESSION['INPUT_FORM'] = [
    "email" => md5(mt_rand()),
    "password" => md5(mt_rand()),
];
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Halaman Login</title>
    <link href="<?=$site_url?>assets/css/bootstrap.css" rel="stylesheet">
    <!-- <link href="css/sb-admin.css" rel="stylesheet" /> -->
    <link href="images/mb.ico" type="image/x-icon" rel="Shortcut Icon" />
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit&hl=id" async defer></script>

<style>
    body, html{
        background-image: url('<?=$site_url?>assets/img/bg.jpg');
        background-size: cover;
        position: center;
    }
</style>

</head>
<body>
    <div style="width: 100%; height: 80px;"></div>
    <div style="width: 100%; height: 80px;"></div>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Silakan Masuk</h3>
                    </div>
                    <div class="panel-body">
                        <form id="myForm">
                            <fieldset>
                                <div class="form-group">
                                    <!-- <input class="form-control" placeholder="" name="recaptcha" type="hidden"/> -->
                                    <input class="form-control" placeholder="Email" name="<?=$_SESSION['INPUT_FORM']["email"]?>" type="email" autofocus="autofocus" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="<?=$_SESSION['INPUT_FORM']["password"]?>" type="password" value="" required />
                                </div>


                                <div class="form-group">
                                    <div class="alert alert-warning text-center captcha-text">
                                        loading captcha
                                    </div>
                                    <div id="recaptcha">
                                    </div>
                                </div>

                                <input type="submit" name="submit_login" value="Login" class="btn btn-lg btn-success btn-block disabled"/>
                                <br>
                                <div>
                                    <a href="register.php"><i class="fas fa-user"></i> Register?</a>
                                    <a href="forgot.php" style="float: right;"><i class="fas fa-envelope"></i> Lupa Password?</a>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../user/assets/js/jquery.js"></script>
    <script src="../user/assets/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/7663bd514c.js"></script>
    <script src="<?=$site_url?>assets/js/fusion.js"></script>
    <!-- <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script> -->
    <!-- <script src="js/sb-admin.js"></script> -->
    <!-- <script src="https://www.google.com/recaptcha/api.js?render=6Ldboa4UAAAAACve6_OmKAnlUfeGxk_MOsab4DEp"></script> -->

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script>
        var captchaExpired = function(){
            grecaptcha.reset();
            $('[name="submit_login"]').addClass("disabled");
        }

        var captchaNextStage = function(response){
            $('[name="submit_login"]').removeClass("disabled");
        }

        var onloadCallback = function() {
            grecaptcha.render("recaptcha",{
                'sitekey': "6LcDpK4UAAAAAJBXd_3L2vV9L_a8qeCdURVH0BDq",
                'callback': captchaNextStage,
                'expired-callback': captchaExpired
            })

            $('.captcha-text').fadeOut();
        };

    // grecaptcha.ready(function() {
    //     $('.captcha-text').fadeOut();
    //     grecaptcha.execute('6Ldboa4UAAAAACve6_OmKAnlUfeGxk_MOsab4DEp', {action: 'comment'}).then(function(token) {
    //      $('#myForm').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');

    //      console.log("token : ", token)
    //  });
    // })

    

    $('#myForm').on("submit", function(e){
        e.preventDefault();
        loading();
        prosesForm();
    });

    function prosesForm(){
        // get form data
        var formData = new FormData($("#myForm")[0]);
        var uri = "proses/user/login-auth.php";

        $.ajax({
            type: "POST",
            url: uri,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(response){

                if( ! response.status){
                    Swal.fire({
                        type: 'error',
                        text: response.message,
                    })

                    captchaExpired();
                    return false;
                }

                Swal.fire({
                    type: 'success',
                    text: response.message,
                })
                setTimeout(function(){
                    console.log(response);
                    window.location.replace(response.data);
                }, 500);
            },
            error: function(err){
                console.log(err);
                Swal.fire({
                    type: 'error',
                    text: err.status + " : " + err.statusText,
                })

                captchaExpired();
            }
        });
    }

    
</script>

</body>
</html>