
<!-- container section end -->

<div class="bikini-bottom" style="position: fixed; bottom: 0; width: 100%; background: #1B2B40; color: #f9f9f9; z-index: 9999;" >
	<div title="Hubungi Kami, FAQ, Perlindungan Privasi, dll" style="border-bottom: 1px solid #0b1b30">
		<i class="fa fa-chevron-up" style="padding: 5px 10px; background: #0b1b30"></i> MORE..
		<div class="pull-right" style="padding-right: 10px; padding-top: 2px;">&copy;Copyright 2019 by Musicblast. All Rights Reserved.</div>
	</div>
	<div style="" class="bottom-info">
		<div class="row">
			<div class="col-xs-6 col-md-4 col-lg-4 text-center">
				<a href="<?=$site_url?>index.php?info=syaratketentuan">Syarat & Ketentuan</a>
			</div>
			<div class="col-xs-6 col-md-4 col-lg-4 text-center">
				<a href="<?=$site_url?>index.php?info=storelist">List Store</a>
			</div>
			<div class="col-xs-6 col-md-4 col-lg-4 text-center">
				<a href="<?=$site_url?>index.php?info=contact">Contact & Information</a>
			</div>
			<div class="col-xs-6 col-md-4 col-lg-4 text-center">
				<a href="<?=$site_url?>index.php?info=faq">Bantuan & Pertanyaan</a>
			</div>
			<div class="col-xs-6 col-md-4 col-lg-4 text-center">
				<a href="<?=$site_url?>index.php?info=privacy">Kebijakan Privasi</a>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	.bikini-bottom{
		height: 24px;
		transition: all 1s;
	}

	.bottom-info .row{
		position: relative;
		padding-top: 10px;
	}

	.bikini-bottom:hover{
		height: 150px;
	}

	.bottom-info a{
		color: #eee;
		background: #0b1b30;
		padding-top:10px;
		padding-bottom: 10px;
		width: 200px;
		display: inline-block;
		margin-bottom: 20px;
		border-radius: 20px;
		transition: all 0.5s;
	}

	.bottom-info a:hover{
		transform: scale(1.1);
		background: #0b0b20
	}




	@media only screen and (max-width: 770px) {
		.bikini-bottom:hover{
			height: 250px;
		}
	}

</style>


</body>

</html>