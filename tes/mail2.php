<?php


require("../vendor/autoload.php");
require("../vendor/constant.php");

$user_to_impersonate = "androidnumber16@gmail.com";
putenv("GOOGLE_APPLICATION_CREDENTIALS=".dirname(__FILE__)."/../vendor/aba-cross-apps-1497604828974-6c576ee7e5c3.json");
$client = new Google_Client();
$client->useApplicationDefaultCredentials();
$client->setSubject($user_to_impersonate);
$client->setApplicationName("My Mailer");
$client->setScopes(["https://www.googleapis.com/auth/gmail.compose"]);
$service = new Google_Service_Gmail($client);
    // Process data
try {
	$strSubject = "Set the email subject here";
	$strRawMessage = "From: Me<".$user_to_impersonate.">\r\n";
	$strRawMessage .= "To: Foo<tfqhisyam@gmail.com>\r\n";
	$strRawMessage .= "CC: Bar<crayskylance@gmail.com>\r\n";
	$strRawMessage .= "Subject: =?utf-8?B?" . base64_encode($strSubject) . "?=\r\n";
	$strRawMessage .= "MIME-Version: 1.0\r\n";
	$strRawMessage .= "Content-Type: text/html; charset=utf-8\r\n";
	$strRawMessage .= "Content-Transfer-Encoding: base64" . "\r\n\r\n";
	$strRawMessage .= "Hello World!" . "\r\n";
        // The message needs to be encoded in Base64URL
	$mime = rtrim(strtr(base64_encode($strRawMessage), '+/', '-_'), '=');
	$msg = new Google_Service_Gmail_Message();
	$msg->setRaw($mime);
        //The special value **me** can be used to indicate the authenticated user.
	$service->users_messages->send("me", $msg);
} catch (Exception $e) {
	print "An error occurred: " . $e->getMessage();
}