function loading(text='Loading') {
	Swal.fire({
		title: text,
		showConfirmButton: false,
		imageUrl: "./assets/img/loader.gif",
		// allowOutsideClick: false,
	})
}

$('form#myForm').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});