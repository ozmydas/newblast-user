<?php
session_start();

require(dirname(__FILE__)."/../../lib/fusion.php");
require(dirname(__FILE__)."/../../lib/datatable.php");

/**********/


$get_datatable = datatable([
		"table" => "release_albums",
		"column" => ['release_albums.id_albums', 'release_albums.album_name', "release_albums.artist", "release_albums.cover_album", "release_albums.tgl_upload", "release_albums.status_album"],
		"column_orderable" => ['release_albums.album_name', "release_albums.artist", null, "release_albums.tgl_upload", "release_albums.status_album", null],
		"column_search" => ['release_albums.album_name', "release_albums.artist", "release_albums.tgl_upload", "release_albums.status_album"],
	], 
	["release_albums.id_user" => $_SESSION["userdata"]["id"]],
	["release_albums.id_albums" => "DESC"]);

// var_dump($get_datatable);

$rows = array();

// looping row buat susun jd isi table
$column_show = ['album_name', "artist", "cover_album", "status_album", "tgl_upload"];
$strlimit = 100;

foreach($get_datatable["data"] as $key1 => $value1):
	$row_data = array();

	foreach($column_show as $key2 => $value2):

		if($value2 == "cover_album"):
			$row_data[] = '<img src="'.'https://drive.google.com/uc?export=view&id='.$value1[$value2].'" width="100px;">';
			continue;
		endif;

		$row_data[] = strlen($value1[$value2]) > $strlimit ? substr($value1[$value2], 0, $strlimit)."..." : $value1[$value2];
	endforeach;

	$row_data[] = '<a href="index.php?view_album&id='.$value1['id_albums'].'"><button class="btn btn-xs btn-primary">lihat detail</button></a>';

	$rows[] = $row_data;
endforeach;

// susun jd array buat json
$result = array(
	"recordsTotal" => $get_datatable["jumlah"],
	"recordsFiltered" => $get_datatable["jumlah"],
	"data" => $rows
);

// kuy kita tampilin
header('Content-Type: application/json');
echo json_encode($result);