<?php
session_start();
global $database, $userdata;
require(dirname(__FILE__)."/../../lib/fusion.php");
require_once(dirname(__FILE__)."/../../lib/gmail.php");
require_once(dirname(__FILE__)."/../../lib/gdrive.php");


$drive = new Gdrive($database);

$view_id = @post($_SESSION['INPUT_FORM']['input_id']);
$userdata = @$_SESSION['userdata'];

// VALIDASI
if(empty($_POST)):
	return json_render(["status" => false, "message" => "Data Kosong", "data" => null]);
endif;

if( empty($view_id)):
	return json_render(["status" => false, "message" => "Invalid ID validation", "data" => null]);
endif;

$edited_data_1 = [
	"bukti" => "",
];

$rules_1 = [
	"id" => $view_id,
	"user_id" => $userdata['id'],
];


if( ! empty($_FILES['bukti']['name'])):
	// JIKA ADA GAMBAR
	$bukti_name = $_FILES["bukti"]["name"];
	$bukti_temp_name = $_FILES["bukti"]["tmp_name"];
	// $bukti_destination = dirname(__FILE__).'/../../upload/bukti/';
	// move_uploaded_file($bukti_temp_name,$bukti_destination.$bukti_name);

	$kirim2 = $drive->upload("master", $bukti_name, $bukti_temp_name, null, $GDRIVE_FOLDERID_BAYAR);
	$bukti_name_id = $kirim2->id;

	$edited_data_1['bukti'] = $bukti_name_id;
endif;


$update_kuy_1 = $database->update('releases', $edited_data_1, $rules_1);


if( ! $update_kuy_1):
	return json_render(["status" => false, "message" => "Terjadi Kesalahan. Silahkan Coba Lagi Nanti atau Hubungi Admin", "data" => $edited_data_1]);
endif;

if( ! $update_kuy_1->rowCount() AND ! $update_kuy_2->rowCount()):
	// return json_render(["status" => false, "message" => "Tidak Ada Perubahan", "data" => $edited_data_1]);
endif;

return json_render(["status" => true, "message" => "Bukti Transfer Berhasil Tersimpan", "data" => $edited_data_1]);
