<?php
session_start();
global $database, $userdata;
require(dirname(__FILE__)."/../../lib/fusion.php");

$view_id = @post($_SESSION['INPUT_FORM']['input_id']);
$userdata = @$_SESSION['userdata'];

// VALIDASI
if(empty($_POST)):
	return json_render(["status" => false, "message" => "Data Kosong", "data" => null]);
endif;

if( empty($view_id)):
	return json_render(["status" => false, "message" => "Invalid ID validation", "data" => null]);
endif;

// SIPAIN DATA
$edited_data_1 = [
	"album_name" => post("title"),
	"artist" => post("artist"),
	"phone" => post("cp"),
	"kmposer" => post("komposer"),
	"pnlis" => post("penulis"),
	"desc_album" => post("deskripsi"),
	"thn_prodksi" => post("thn_prdksi"),
];


// DIMANA DIA
$rules_1 = [
	"id_albums" => $view_id,
	"id_user" => $userdata['id'],
];


/**/


if( ! empty($_FILES['image']['name'])):
	// JIKA ADA GAMBAR
	$file_name1 = $_FILES["image"]["name"];
	$temp_name1 = $_FILES["image"]["tmp_name"];
	$destination1 = dirname(__FILE__).'/../../upload/cover_album/';
	move_uploaded_file($temp_name1,$destination1.$file_name1);

	$edited_data_1['cover_album'] = $file_name1;
endif;


if( ! empty($_FILES['ktp']['name'])):
	// JIKA ADA GAMBAR
	$ktp_name = $_FILES["ktp"]["name"];
	$ktp_temp_name = $_FILES["ktp"]["tmp_name"];
	$ktp_destination = dirname(__FILE__).'/../../upload/ktp/';
	move_uploaded_file($ktp_temp_name,$ktp_destination.$ktp_name);

	$edited_data_1['ktp'] = $ktp_name;
endif;

// UP LAGU

if( ! empty($_FILES['song']['name'][0])):
	$total = count($_FILES['song']['name']);
	$songg = "";
            // Loop through each file
	for( $i=0 ; $i < $total ; $i++ ) {

            //Get the temp file path
		$tmpFilePath = $_FILES['song']['tmp_name'][$i];

            //Make sure we have a file path
		if ($tmpFilePath != ""){
                    //Setup our new file path
			$newFilePath = dirname(__FILE__).'/../../upload/song_album/' . $_FILES['song']['name'][$i];

                    //Upload the file into the temp dir
			if(move_uploaded_file($tmpFilePath, $newFilePath)) {

				$songg .= $_FILES['song']['name'][$i].',';
			}
		}
	}
	$songgfinal = rtrim($songg, ',');

	$edited_data_1['songs'] = $songgfinal;
endif;
/**/

$update_kuy_1 = $database->update('release_albums', $edited_data_1, $rules_1);

if( ! $update_kuy_1):
	return json_render(["status" => false, "message" => "Terjadi Kesalahan. Silahkan Coba Lagi Nanti atau Hubungi Admin", "data" => $edited_data_1]);
endif;

if( ! $update_kuy_1->rowCount()):
	return json_render(["status" => false, "message" => "Tidak Ada Perubahan", "data" => $edited_data_1]);
endif;

return json_render(["status" => true, "message" => "Perubahan Berhasil Tersimpan", "data" => $edited_data_1]);