<?php
session_start();
global $database, $userdata;

require_once(dirname(__FILE__)."/../../lib/fusion.php");
require_once(dirname(__FILE__)."/../../lib/gmail.php");

// VALIDASI
if(empty($_POST)):
	return json_render(["status" => false, "message" => "Data Kosong", "data" => null]);
endif;


// SET VARIABLE
$id_user = $_SESSION['userdata']['id'];	
$title = $_POST['title'];
$artist = $_POST['artist'];
$ft_artist = $_POST['ft_artist'];
$bhsa = $_POST['bhsa'];
$genres = $_POST['genres'];
$press_release = $_POST['press_release'];
$cp = $_POST['cp'];
$ig = $_POST['ig'];
$fb = $_POST['fb'];
$pnls = $_POST ['penulis'];
$kmpsr = $_POST ['komposer'];
$tpr = $_POST ['thn_prdksi'];
$sl = @$_POST['song_lyric'];
$start_date = $_POST['tgl_release'];
$date = date("Y-m-d");

$sd = 'start_date';
$ui = 'user_id';
$st = 'status';


if ($_POST['label'] == "custom") {
	$labelname = $_POST['lebelname'];
	$price = 200000;
}else if($_POST['label'] == "mblast"){
	$labelname = 'Musicblast.id';
	$price = 100000;
} else {
	$labelname = 'Musicblast.id';
	$price = 100000;
}


if (isset($_POST['store'])) {
	$storei = "";
	foreach($_POST['store'] as $key => $item){
		$storei .= $item.', ';
	}
	$storefinal = rtrim($storei, ', ');
}else{
	$storefinal = null;
}

// SAVE GAMBAR
$file_name1 = $_FILES["image"]["name"];
$temp_name1 = $_FILES["image"]["tmp_name"];
$destination1 = dirname(__FILE__).'/../../upload/cover/';
move_uploaded_file($temp_name1,$destination1.$file_name1);

$ktp_name = $_FILES["ktp"]["name"];
$ktp_temp_name = $_FILES["ktp"]["tmp_name"];
$ktp_destination = dirname(__FILE__).'/../../upload/ktp/';
move_uploaded_file($ktp_temp_name,$ktp_destination.$ktp_name);

// SAVE LAGU
$song_name = $_FILES["song"]["name"];
$song_temp_name = $_FILES["song"]["tmp_name"];
$song_destination = dirname(__FILE__).'/../../upload/song/';
move_uploaded_file($song_temp_name,$song_destination.$song_name);


// SAVE KE DB
$simpan = $database->query("INSERT INTO releases
	(title,artist,ft_artist,bhsa,genre_id,$sd,upload_date,label,penulis,komposer,thn_prdksi,press_release,$ui,$st,cp,ig,fb,store, ktp) 
	VALUES
	('$title','$artist','$ft_artist','$bhsa','$genres','$start_date','$date','$labelname','$pnls','$kmpsr','$tpr','$press_release','$id_user','Belum Terkonfirmasi','$cp','$ig','$fb','$storefinal', '$ktp_name')");

if(empty($database->id())):
	return json_render(["status" => false, "message" => "Upload Lagu Gagal", "data" => $_POST]);
endif;

$id_release = $database->id();

$sql_release_artworks = $database->query("INSERT INTO release_artworks (image,release_id) VALUES ('$file_name1', '$id_release') ");
$sql_release_songs = $database->query("INSERT INTO release_songs (release_id,file,lirik) VALUES ('$id_release', '$song_name', '$sl') "); 


/** send email here **/
$gugel = new Gugel($database);
$vari = [
	"mail_subject" => "Single Tersimpan - ".@$title,
	"mail_content" => 'Terima Kasih. Single Berhasil Tersimpan :<br/><br/>
	Judul : '.@$title.'<br/>
	Artis : '.@$artist.' '.@$ft_artist.'<br/>
	Status : Belum Terkonfirmasi<br/><br/>
	Untuk detail dan konfirmasi, kunjungi <b>halaman Publisher > Discography Single</b> di Akun MusicBlast Kamu.
	'
];

$html = render_template("mail/mail.php", $vari);
$tujuan = $_SESSION['userdata']['email'];;
$subjek = $vari["mail_subject"];
$isi_pesan = $html;

$kirim = $gugel->send("master", $tujuan, $subjek, $isi_pesan);
/** end here **/


return json_render(["status" => true, "message" => "Upload Lagu Berhasil", "data" => null]);
