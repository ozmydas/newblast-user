<?php
session_start();
global $database, $userdata;

require_once(dirname(__FILE__)."/../../lib/fusion.php");
require_once(dirname(__FILE__)."/../../lib/gmail.php");

// VALIDASI
if(empty($_POST)):
	return json_render(["status" => false, "message" => "Data Kosong", "data" => null]);
endif;

// SET VARIABLE
$id_user = $_SESSION['userdata']['id'];
$album_name = $_POST['an'];
$artist = $_POST['artist'];
$genres = $_POST['genres'];
$tgl_upload = date("Y-m-d");
$tgl_release = $_POST['tgl_release'];
$desc_album = $_POST['da'];
$phone = $_POST['phone'];
$pnls = $_POST ['pnlis'];
$kmpsr = $_POST ['kmposer'];
$tprd = $_POST ['thn_prodksi'];


if ($_POST['label'] == "custom") {
	$labelname = $_POST['lebelname'];
	$price = 200000;
}else if($_POST['label'] == "mblast"){
	$labelname = 'Musicblast.id';
	$price = 100000;
} else {
	$labelname = 'Musicblast.id';
	$price = 100000;
}


if (isset($_POST['store'])) {
	$storei = "";
	foreach($_POST['store'] as $key => $item){
		$storei .= $item.', ';
	}
	$storefinal = rtrim($storei, ', ');
}else{
	$storefinal = null;
}

$file_name1 = $_FILES["image"]["name"];
$temp_name1 = $_FILES["image"]["tmp_name"];
$destination1 = dirname(__FILE__).'/../../upload/cover_album/';


$ktp_name = $_FILES["ktp"]["name"];
$ktp_temp_name = $_FILES["ktp"]["tmp_name"];
$ktp_destination = dirname(__FILE__).'/../../upload/ktp/';

  // Count # of uploaded files in array
$total = count($_FILES['song']['name']);

$songg = "";
            // Loop through each file
for( $i=0 ; $i < $total ; $i++ ) {

            //Get the temp file path
	$tmpFilePath = $_FILES['song']['tmp_name'][$i];

            //Make sure we have a file path
	if ($tmpFilePath != ""){
                    //Setup our new file path
		$newFilePath = dirname(__FILE__).'/../../upload/song_album/' . $_FILES['song']['name'][$i];

                    //Upload the file into the temp dir
		if(move_uploaded_file($tmpFilePath, $newFilePath)) {

			$songg .= $_FILES['song']['name'][$i].',';
		}
	}
}
$songgfinal = rtrim($songg, ',');

// SAVE KE DB
$simpan = $database->query("INSERT INTO release_albums (id_user,album_name,desc_album,artist,g_id,tgl_upload,tgl_release,layanan,label,phone,cover_album,pnlis,kmposer,thn_prodksi,songs, ktp) VALUES ('$id_user','$album_name','$desc_album','$artist','$genres','$tgl_upload','$tgl_release','$storefinal','$labelname','$phone','$file_name1','$pnls','$kmpsr','$tprd','$songgfinal', '$ktp_name')");

if(empty($database->id())):
	return json_render(["status" => false, "message" => "Upload Album Gagal", "data" => $_POST]);
endif;


move_uploaded_file($ktp_temp_name,$ktp_destination.$ktp_name);
move_uploaded_file($temp_name1,$destination1.$file_name1);


/** send email here **/
$gugel = new Gugel($database);
$vari = [
	"mail_subject" => "Album Tersimpan - ".@$album_name,
	"mail_content" => 'Terima Kasih. Single Berhasil Tersimpan :<br/><br/>
	Judul : '.@$album_name.'<br/>
	Artis : '.@$artist.' '.@$ft_artist.'<br/>
	Status : Belum Terkonfirmasi<br/><br/>
	Untuk detail dan konfirmasi, kunjungi <b>halaman Publisher > Discography Album</b> di Akun MusicBlast Kamu.
	'
];

$html = render_template("mail/mail.php", $vari);
$tujuan = $_SESSION['userdata']['email'];;
$subjek = $vari["mail_subject"];
$isi_pesan = $html;

$kirim = $gugel->send("master", $tujuan, $subjek, $isi_pesan);
/** end here **/


return json_render(["status" => true, "message" => "Upload Album Berhasil", "data" => null]);