<?php
session_start();
global $database, $userdata;
require(dirname(__FILE__)."/../../lib/fusion.php");

$view_id = @post($_SESSION['INPUT_FORM']['input_id']);
$userdata = @$_SESSION['userdata'];

// VALIDASI
if(empty($_POST)):
	return json_render(["status" => false, "message" => "Data Kosong", "data" => null]);
endif;

if( empty($view_id)):
	return json_render(["status" => false, "message" => "Invalid ID validation", "data" => null]);
endif;

// SIPAIN DATA
$edited_data_1 = [
	"title" => post("title"),
	"artist" => post("artist"),
	"bhsa" => post("bhsa"),
	"cp" => post("cp"),
	"fb" => post("fb"),
	"ft_artist" => post("ft_artist"),
	"ig" => post("ig"),
	"komposer" => post("komposer"),
	"penulis" => post("penulis"),
	"press_release" => post("press_release"),
	"thn_prdksi" => post("thn_prdksi"),
];

$edited_cover = [];

$edited_data_2 = [
	"lirik" => post("song_lyric"),
];


// DIMANA DIA
$rules_1 = [
	"id" => $view_id,
	"user_id" => $userdata['id'],
];

$rules_2 = [
	"release_id" => $view_id
];

$update_kuy_3 = true;

if( ! empty($_FILES['image']['name'])):
	// JIKA ADA GAMBAR
	$file_name1 = $_FILES["image"]["name"];
	$temp_name1 = $_FILES["image"]["tmp_name"];
	$destination1 = dirname(__FILE__).'/../../upload/cover/';
	move_uploaded_file($temp_name1,$destination1.$file_name1);

	$edited_cover['image'] = $file_name1;

	$update_kuy_3 = $database->update('release_artworks', $edited_cover, $rules_2);
endif;


if( ! empty($_FILES['ktp']['name'])):
	// JIKA ADA GAMBAR
	$ktp_name = $_FILES["ktp"]["name"];
	$ktp_temp_name = $_FILES["ktp"]["tmp_name"];
	$ktp_destination = dirname(__FILE__).'/../../upload/ktp/';
	move_uploaded_file($ktp_temp_name,$ktp_destination.$ktp_name);

	$edited_data_1['ktp'] = $ktp_name;
endif;

if( ! empty($_FILES['song']['name'])):
	// JIKA ADA LAGU
	$song_name = $_FILES["song"]["name"];
	$song_temp_name = $_FILES["song"]["tmp_name"];
	$song_destination = dirname(__FILE__).'/../../upload/song/';
	move_uploaded_file($song_temp_name,$song_destination.$song_name);

	$edited_data_2['file'] = $song_name;
endif;



$update_kuy_1 = $database->update('releases', $edited_data_1, $rules_1);
$update_kuy_2 = $database->update('release_songs', $edited_data_2, $rules_2);

if( ! $update_kuy_1 OR ! $update_kuy_2):
	return json_render(["status" => false, "message" => "Terjadi Kesalahan. Silahkan Coba Lagi Nanti atau Hubungi Admin", "data" => array_merge($edited_data_1, $edited_data_2, $edited_cover)]);
endif;

if( ! $update_kuy_1->rowCount() AND ! $update_kuy_2->rowCount()):
	// return json_render(["status" => false, "message" => "Tidak Ada Perubahan", "data" => array_merge($edited_data_1, $edited_data_2, $edited_cover)]);
endif;

return json_render(["status" => true, "message" => "Perubahan Berhasil Tersimpan", "data" => array_merge($edited_data_1, $edited_data_2, $edited_cover)]);