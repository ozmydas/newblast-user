<?php
session_start();
global $database, $userdata;

require(dirname(__FILE__)."/../../lib/fusion.php");

$id_penerima = empty($post['recipient']) ? 1 : intval(@$post['recipient']);

$insert_data = [
	'id_pengirim' => $_SESSION['userdata']['id'],
	'id_penerima' => $id_penerima,
	'tanggal' => date('Y-m-d H:i:s'),
	'subyek_pesan' => post('subject'),
	'isi_pesan' => post('message'),
];

$database->insert('pesan', $insert_data);

if( ! $database->id()):
	return json_render(["status" => false, "message" => "Gagal Mengirim Pesan", "data" => null]);
endif;

return json_render(["status" => true, "message" => "Pesan Terkirim", "data" => $insert_data]);