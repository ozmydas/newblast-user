<?php
session_start();
global $database, $userdata;

require(dirname(__FILE__)."/../../lib/fusion.php");

// AMBIL PESAN
$result = $database->select('pesan', [
	'[>]users(pengirim)' => ['pesan.id_pengirim' => 'id'],
	'[>]users(penerima)' => ['pesan.id_penerima' => 'id'],
], [
	'pesan.id_pesan',
	'pesan.id_pengirim',
	'pesan.id_penerima',
	'penerima.name(penerima)',
	'pengirim.name(pengirim)',
	'pesan.tanggal',
	'pesan.subyek_pesan',
	'pesan.isi_pesan',
], [
	'id_pesan' => $_GET['id_pesan'],
	'id_penerima' => $_SESSION['userdata']['id'],
]);

if(empty($result)):
	return json_render(["status" => false, "message" => "Gagal Mengambil Pesan", "data" => null]);
endif;

// UPDATE JD READ

$database->update('pesan', [
	'sudah_dibaca' => 'sudah'
],[
	'id_pesan' => $_GET['id_pesan'],
	'id_penerima' => $_SESSION['userdata']['id'],
]);

return json_render(["status" => true, "message" => "Berhasil Mengambil Pesan", "data" => $result[0]]);