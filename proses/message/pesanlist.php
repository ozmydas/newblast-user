<?php
session_start();

require(dirname(__FILE__)."/../../lib/fusion.php");
require(dirname(__FILE__)."/../../lib/datatable.php");

/**********/


$get_datatable = datatable([
	"table" => "pesan",
	"column" => ['pesan.id_pesan', 'pesan.tanggal', 'pesan.subyek_pesan', 'pesan.sudah_dibaca', 'users.name'],
	"column_orderable" => ['pesan.tanggal', 'users.name', 'pesan.subyek_pesan', 'pesan.sudah_dibaca', null],
	"column_search" => ['pesan.tanggal', 'users.name', 'pesan.subyek_pesan', 'pesan.sudah_dibaca'],
], 
["pesan.id_penerima" => $_SESSION["userdata"]["id"]],
["id_pesan" => "DESC"],
[
	"[>]users" => ["id_pengirim" => "id"],
],
false
);

// var_dump($get_datatable);

$rows = array();

// looping row buat susun jd isi table
$column_show = ['tanggal', 'name', 'subyek_pesan', 'sudah_dibaca'];
foreach($get_datatable["data"] as $key1 => $value1):
	$row_data = array();

	foreach($column_show as $key2 => $value2):
		if($value1["sudah_dibaca"] == "sudah"):
			$row_data[] = $value1[$value2];
		else:
			$row_data[] = "<b>".$value1[$value2]."</b>";
		endif;
	endforeach;

	$row_data[] = '<button class="btn btn-xs btn-primary" onclick="bacapesan('.$value1['id_pesan'].')">lihat pesan</button>';

	$rows[] = $row_data;
endforeach;

// susun jd array buat json
$result = array(
	"recordsTotal" => $get_datatable["jumlah"],
	"recordsFiltered" => $get_datatable["jumlah"],
	"data" => $rows
);

// kuy kita tampilin
header('Content-Type: application/json');
echo json_encode($result);