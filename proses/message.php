<?php
session_start();

require(dirname(__FILE__)."/../../lib/fusion.php");
require(dirname(__FILE__)."/../../lib/datatable.php");

/**********/


$get_datatable = datatable([
		"table" => "releases",
		"column" => ['releases.id', 'releases.title', "releases.artist", "release_artworks.image", "releases.upload_date", "releases.status"],
		"column_orderable" => ['releases.title', "releases.artist", null, "releases.upload_date", "releases.status", null],
		"column_search" => ['releases.title', "releases.artist", "releases.upload_date", "releases.status"],
	], 
	["releases.user_id" => $_SESSION["userdata"]["id"]],
	"id",
	[
		"[>]release_artworks" => ["id" => "release_id"],
	]);

// var_dump($get_datatable);

$rows = array();

// looping row buat susun jd isi table
$column_show = ['title', "artist", "image", "upload_date", "status"];
foreach($get_datatable["data"] as $key1 => $value1):
	$row_data = array();

	foreach($column_show as $key2 => $value2):

		if($value2 == "image"):
			$row_data[] = '<img src="'.'./upload/cover/'.$value1[$value2].'" height="40px;">';
			continue;
		endif;

		$row_data[] = $value1[$value2];
	endforeach;

	$row_data[] = '<button class="btn btn-xs btn-primary">lihat detail</button>';

	$rows[] = $row_data;
endforeach;

// susun jd array buat json
$result = array(
	"recordsTotal" => $get_datatable["jumlah"],
	"recordsFiltered" => $get_datatable["jumlah"],
	"data" => $rows
);

// kuy kita tampilin
header('Content-Type: application/json');
echo json_encode($result);