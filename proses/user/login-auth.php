<?php
session_start();

require(dirname(__FILE__)."/../../lib/fusion.php");

$username = post(@$_SESSION['INPUT_FORM']["email"]);
$passw = post(@$_SESSION['INPUT_FORM']["password"]);
$recaptcha = @$_POST["g-recaptcha-response"];

/****/

$verify = curl_post('https://www.google.com/recaptcha/api/siteverify', ['secret' => $RECAPTCHA_SECRET, 'response' => $recaptcha]);
$array_response = json_decode($verify, true);

if( ! $array_response["success"]):
	return json_render(["status" => false,"message" => "ReCaptcha Validation Error. Please Reload Page And Try Again","data" => $array_response]);
endif;

/****/

$data = $database->select('users', [
	'id', 'email', 'name', 'password', 'role'
], [
	'email' => $username
]);

if(empty($data)):
	return json_render(["status" => false, "message" => "Email Tidak terdaftar", "data" => null]);
endif;

if( ! password_verify($passw, $data[0]["password"])):
	return json_render(["status" => false, "message" => "Password Salah!", "data" => null]);
endif;

switch ($data[0]["role"]) {
	case 'user':
	$_SESSION["userdata"] = [
		"id" => $data[0]['id'],
		"name" => $data[0]['name'],
		"email" => $data[0]['email'],
		"token" => sha1(rand()),
	];
	$redir = "./index.php?dashboard";
	break;
	case 'admin':
	$_SESSION["admindata"] = [
		"id" => $data[0]['id'],
		"name" => $data[0]['name'],
		"email" => $data[0]['email'],
		"token" => sha1(rand()),
	];
	$redir = "../admin/index.php";
	break;
	default:
	$redir = "";
}


return json_render(["status" => true, "message" => "Login Sukses!", "data" => $redir]);
