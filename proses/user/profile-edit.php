<?php
session_start();

require(dirname(__FILE__)."/../../lib/fusion.php");

$update_data = [
	'name' => post('fullname'),
	// 'email' => post('email'),
	'no_hp' => post('hp'),
	'bank' => post('bankname'),
	'no_rek' => post('bankrek'),
];


if( ! empty($_FILES['foto']['name'])):
	$upload = upfile([
		'input' => 'foto',
		'path' => dirname(__FILE__).'/../../upload/profil/',
		'random' => TRUE,
	]);

	if( ! empty($upload['error'])):
		return json_render(["status" => false, "message" => "Upload Error", "data" => $upload]);
	endif;

	$update_data['foto'] = $upload['data']['filename'];
endif;


$proses = $database->update('users', $update_data, [
	'id' => $_SESSION['userdata']['id'],
]);

if( ! $proses->rowCount())
	return json_render(["status" => false, "message" => "Tidak Ada Perubahan", "data" => $_POST]);

return json_render(["status" => true, "message" => "Data Profile Tersimpan", "data" => null]);
