<?php
session_start();

require(dirname(__FILE__)."/../../lib/fusion.php");
require(dirname(__FILE__)."/../../lib/gmail.php");

$email = post(@$_SESSION['INPUT_FORM']["email"]);
$new_password = post(@$_SESSION['INPUT_FORM']["new_password"]);
$re_password = post(@$_SESSION['INPUT_FORM']["re_password"]);
// $recaptcha = @$_POST["g-recaptcha-response"];

/****/

// $verify = curl_post('https://www.google.com/recaptcha/api/siteverify', ['secret' => $RECAPTCHA_SECRET, 'response' => $recaptcha]);
// $array_response = json_decode($verify, true);

// if( ! $array_response["success"]):
// 	return json_render(["status" => false,"message" => "ReCaptcha Validation Error. Please Reload Page And Try Again","data" => $array_response]);
// endif;

/****/

// CEK PASS
if($new_password !== $re_password):
	return json_render(["status" => false, "message" => "Password Tidak Sama", "data" => null]);
endif;

// CEK AKUN
$data = $database->select('users', [
	'id', 'email'
], [
	'email' => $email
]);

if(empty($data)):
	return json_render(["status" => false, "message" => "Tidak Valid", "data" => null]);
endif;

// SIMPAN
$newpass = password_hash($new_password, PASSWORD_DEFAULT);

$data = $database->update('users', [
	'password'=> $newpass,
], [
	'id' => $data[0]["id"],
	'email' => $data[0]["email"]
]);

return json_render(["status" => true, "message" => "Reset Password Berhasil", "data" => null]);
