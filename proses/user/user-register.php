<?php
session_start();

require(dirname(__FILE__)."/../../lib/fusion.php");
require(dirname(__FILE__)."/../../lib/gmail.php");

$name = post(@$_SESSION['INPUT_FORM']["name"]);
$no_hp = post(@$_SESSION['INPUT_FORM']["no_hp"]);
$email = post(@$_SESSION['INPUT_FORM']["email"]);
$password = post(@$_SESSION['INPUT_FORM']["password"]);
$recaptcha = @$_POST["g-recaptcha-response"];


$password_hashed = password_hash("$password", PASSWORD_DEFAULT);

/****/

$verify = curl_post('https://www.google.com/recaptcha/api/siteverify', ['secret' => $RECAPTCHA_SECRET, 'response' => $recaptcha]);
$array_response = json_decode($verify, true);

if( ! $array_response["success"]):
	return json_render(["status" => false,"message" => "ReCaptcha Validation Error. Please Reload Page And Try Again","data" => $array_response]);
endif;

/****/

// cek apa sudah ada
$ada = $database->count('users', ['email ' => $email]);

if($ada):
	return json_render(["status" => false,"message" => "Email Telah Terdaftar","data" => null]);
endif;

$data = $database->insert('users', [
	'name' => $name,
	'no_hp' => $no_hp,
	'email' => $email,
	'password' => $password_hashed,
	'role' => 'user',
]);

if( ! @$data->rowCount()):
	return json_render(["status" => false, "message" => "Terjadi Kesalahan", "data" => null]);
endif;

/** kirim email **/

$gugel = new Gugel($database);
$vari = [
	"mail_subject" => "Register Berhasil",
	"mail_content" => 'Hi '.$name.',<br/>Terima Kasih Telah Melakukan Pendaftaran Akun MusicBlast.'
];

$html = render_template("mail/mail.php", $vari);
$tujuan = $email;
$subjek = $vari["mail_subject"];
$isi_pesan = $html;

$gugel->send("master", $tujuan, $subjek, $isi_pesan);

/****/

$redir = "login.php";
return json_render(["status" => true, "message" => "Pendaftaran Sukses!", "data" => $redir]);
