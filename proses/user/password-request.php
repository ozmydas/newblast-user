<?php
session_start();

require(dirname(__FILE__)."/../../lib/fusion.php");
require(dirname(__FILE__)."/../../lib/gmail.php");

$email = post(@$_SESSION['INPUT_FORM']["email"]);
$recaptcha = @$_POST["g-recaptcha-response"];

/****/

$verify = curl_post('https://www.google.com/recaptcha/api/siteverify', ['secret' => $RECAPTCHA_SECRET, 'response' => $recaptcha]);
$array_response = json_decode($verify, true);

if( ! $array_response["success"]):
	return json_render(["status" => false,"message" => "ReCaptcha Validation Error. Please Reload Page And Try Again","data" => $array_response]);
endif;

/****/

$data = $database->select('users', [
	'id', 'email'
], [
	'email' => $email
]);

if(empty($data)):
	return json_render(["status" => false, "message" => "Email Tidak terdaftar", "data" => null]);
endif;

// SEND EMAIL
$link = str_replace('proses/user/', '', base_url())."reset.php?email=".$data[0]["email"]."&token=".sha1($data[0]["email"].date("Y-m-d"));
$gugel = new Gugel($database);
$vari = [
	"mail_subject" => "Reset Password",
	"mail_content" => 'Silahkan klik link berikut untuk me-reset password : <br/><br/><a href="'.$link.'">'.$link.'</a>'
];

$html = render_template("mail/mail.php", $vari);
$tujuan = $data[0]["email"];
$subjek = $vari["mail_subject"];
$isi_pesan = $html;

/****/

$kirim = $gugel->send("master", $tujuan, $subjek, $isi_pesan);

return json_render($kirim);
