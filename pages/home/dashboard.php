<?php

$data["page_title"] = "Dashboard User";

/** QUERY SQL **/
$data["total_single"] = $database->count("releases", ["user_id" => $userdata["id"]]); 
$data["total_album"] = $database->count("release_albums", ["id_user" => $userdata["id"]]);

$data["latest_single"] = $database->select("releases", ["[>]release_artworks" => ["id" => "release_id"]], ['releases.id', 'releases.title', "release_artworks.image"], [
	"releases.user_id" => $_SESSION["userdata"]["id"],
	"LIMIT" => [0, 8],
	"ORDER" => ["releases.id" => "DESC"]
]);

$data["latest_album"] = $database->select("release_albums",['id_albums', 'album_name', "cover_album"], [
	"id_user" => $_SESSION["userdata"]["id"],
	"LIMIT" => [0, 8],
	"ORDER" => ["id_albums" => "DESC"]
]);

// var_dump($data["latest_single"]);

/** VIEW TEMPLATE CONTENT **/
load_template("home/dashboard", $data);
