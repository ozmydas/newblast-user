<?php

$label = $_GET['info'];
$content = $database->select('content', '*', ['label' => $label]);
$data['page_title'] = ucfirst($label);

if(empty($content))
	return load_template("home/notfound", $data); 

$data['page_title'] = $content[0]['title'];
$data['content'] = $content[0]['content'];

/** VIEW TEMPLATE CONTENT **/
load_template("home/generic", $data); 