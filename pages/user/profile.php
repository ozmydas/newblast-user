<?php

$data["page_title"] = "My Profile";

/** QUERY SQL **/
$getdata = $database->select("users","*", ["id" => $userdata["id"]]); 
$data['foto'] = @$getdata[0]["foto"];
$data["profiles"] = [
	[
		"Nama Lengkap" => @$getdata[0]["name"],
		"Email" => @$getdata[0]["email"],
		"No. Handphone" => @$getdata[0]["no_hp"],
	],[
		"Nama Bank" => @$getdata[0]["bank"],
		"No. Rekening" => @$getdata[0]["no_rek"],
	],
];


/** VIEW TEMPLATE CONTENT **/
load_template("user/profile", $data);
