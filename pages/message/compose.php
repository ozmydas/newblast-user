<?php

/** QUERY SQL **/
$data["page_title"] = "My Messages";
$data["opened_message"] = FALSE;

if( ! empty($_GET['message'])):
	$result = $database->select('pesan', [
		"[>]users" => ["id_pengirim" => "id"],
	], [
		'pesan.id_pesan', 'pesan.tanggal', 'pesan.subyek_pesan', 'pesan.sudah_dibaca', 'pesan.id_pengirim', 'pesan.isi_pesan', 'users.name'
	], [
		'pesan.id_pesan' => $_GET['message'],
		'pesan.id_penerima' => $userdata['id'],
	]);

	if(count($result)):
		$data['opened_message'] = $result[0];

		// UBAH JD SUDAH DIBACA
		$database->update('pesan', [
			'sudah_dibaca' => 'sudah'
		],[
			'id_pesan' => $_GET['message'],
		]);
	endif;
endif;


/** VIEW TEMPLATE CONTENT **/
load_template("message/compose", $data); // "blank" itu dari template/content/blank.php, biar manggilnya gampang wkwk
