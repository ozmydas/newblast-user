<?php

/** QUERY SQL **/
$data["page_title"] = "Report Request";

$lagus = $database->select('releases', ['title', 'artist'], ['user_id' => $_SESSION['userdata']['id']]);
$albums = $database->select('release_albums', ['album_name', 'artist', 'songs'], ['id_user' => $_SESSION['userdata']['id']]);

$songs_available = [];

foreach($lagus as $key => $value):
	array_push($songs_available, [
		'value' => "single///".$value['artist']."///".$value['title'],
		'name' => $value['artist']." (single) - ".$value['title'],
	]);
endforeach;

foreach($albums as $key => $value):
	$lagu = explode(",", $value['songs']);
	array_pop($lagu);

	$value_parent = $value;
	foreach($lagu as $key => $value):
		$judul = str_replace(".".pathinfo($value, PATHINFO_EXTENSION), "", $value);
		array_push($songs_available,  [
			'value' => "album///".$value_parent['artist']."///".$judul."///".$value_parent['album_name'],
			'name' => $value_parent['artist']." (".$value_parent['album_name'].") - ".$judul,
		]);
	endforeach;
endforeach;

// print_r($songs_available); exit();
$data['songs_available'] = $songs_available;
/** VIEW TEMPLATE CONTENT **/
load_template("service/report", $data);
