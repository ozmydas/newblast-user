<?php

/** QUERY SQL **/
$data["page_title"] = "Service Item";

$data["stores"] = $database->select("service_items", ["name", "description", "price", "status"], ["ORDER" => ["id" => "ASC"]]); 

/** VIEW TEMPLATE CONTENT **/
load_template("service/item", $data); // "blank" itu dari template/content/blank.php, biar manggilnya gampang wkwk
