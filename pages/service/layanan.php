<?php

/** QUERY SQL **/
$data["tes"] = "TESS BOSSQUE";
$data["page_title"] = "Layanan";

$data["services"] = [
	[
		"icon" => "fa fa-rocket",
		"name" => "DISTRIBUSI",
		"link" => "#",
		"img" => $site_url."assets/img/cd3.png",
	],
	[
		"icon" => "fa fa-rocket",
		"name" => "PROMOSI ADS",
		"link" => "#",
		"img" => $site_url."assets/img/sosmed2.png",
	],
	[
		"icon" => "fa fa-rocket",
		"name" => "PROMOSI RADIO",
		"link" => "#",
		"img" => $site_url."assets/img/audio1.png",
	],
	[
		"icon" => "fa fa-rocket",
		"name" => "MIXING MASTERING",
		"link" => "#",
		"img" => $site_url."assets/img/recording1.png",
	],
	[
		"icon" => "fa fa-rocket",
		"name" => "VIDEO PRODUCTION",
		"link" => "#",
		"img" => $site_url."assets/img/video1.png",
	],
	[
		"icon" => "fa fa-rocket",
		"name" => "LAINNYA",
		"link" => "#",
		"img" => $site_url."assets/img/music1.png",
	],
];

/** VIEW TEMPLATE CONTENT **/
load_template("service/layanan", $data); // "blank" itu dari template/content/blank.php, biar manggilnya gampang wkwk
