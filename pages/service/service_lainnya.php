<?php

/** QUERY SQL **/
$data["page_title"] = "Service";
$data["subtitle"] = "Service Lainnya";

$data["services"] = [
	[
		"icon" => "fa fa-external-link",
		"name" => "KEMBALI",
		"link" => $site_url.'index.php?service',
		"img" => $site_url."assets/img/music1.png",
	],
	[
		"icon" => "fa fa-bell",
		"name" => "LAUNCHING ORGANIZER",
		"link" => "#",
		"img" => $site_url."assets/img/launch.png",
	],
	[
		"icon" => "fa fa-circle-o",
		"name" => "CETAK CD ALBUM",
		"link" => "#",
		"img" => $site_url."assets/img/cd.png",
	],
	[
		"icon" => "fa fa-laptop",
		"name" => "WEB DESIGN",
		"link" => "#",
		"img" => $site_url."assets/img/webdesain.png",
	],
	[
		"icon" => "fa fa-mobile-phone",
		"name" => "BAND APP",
		"link" => $site_url.'index.php?service-other',
		"img" => $site_url."assets/img/applikasi.png",
	],
	[
		"icon" => "fa fa-male",
		"name" => "KAOS (T-SHIRT)",
		"link" => "#",
		"img" => $site_url."assets/img/tshirt.png",
	],
];

/** VIEW TEMPLATE CONTENT **/
load_template("service/layanan", $data); // "blank" itu dari template/content/blank.php, biar manggilnya gampang wkwk
