<?php

/** QUERY SQL **/
$data["page_title"] = "Store List";

// $data["stores"] = $database->select("stores", ["name", "description"], ["ORDER" => ["id" => "DESC"]]);

$data["stores"] = [

	[
		'link' => 'https://www.amazon.com/MP3-Music-Download/b?ie=UTF8&node=163856011', 
		'img' => 'http://musicblast.id/mblast/newblast/img/store/amazon.png'
	],
	[
		'link' => 'https://www.spotify.com/id/', 
		'img' => 'http://musicblast.id/mblast/newblast/img/store/spotify.png'
	],
	[
		'link' => 'https://www.joox.com/id', 
		'img' => 'http://musicblast.id/mblast/newblast/img/store/joox.png'
	],
	[
		'link' => 'https://www.apple.com/id/itunes/', 
		'img' => 'http://musicblast.id/mblast/newblast/img/store/itunes.png'
	],
	[
		'link' => 'https://play.google.com/music/listen?u=0#', 
		'img' => 'http://musicblast.id/mblast/newblast/img/store/google.png'
	],
	[
		'link' => 'https://youtube.com', 
		'img' => 'http://musicblast.id/mblast/newblast/img/store/youtube.png'
	],
	[
		'link' => 'https://www.iheart.com', 
		'img' => 'http://musicblast.id/mblast/newblast/img/store/iheart.png'
	],
	[
		'link' => 'http://shazamusic.club', 
		'img' => 'http://musicblast.id/mblast/newblast/img/store/shazam.png'
	],

];

/** VIEW TEMPLATE CONTENT **/
load_template("service/storelist", $data);
