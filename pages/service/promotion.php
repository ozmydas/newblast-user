<?php

/** QUERY SQL **/
$data["page_title"] = "Promotion";
$data["subtitle"] = "Promosikan Musik Kamu";

$data["services"] = [
	[
		"icon" => "fa fa-pencil-square-o",
		"name" => "DISTRIBUSI",
		"link" => "#",
		"img" => $site_url."assets/img/cd3.png",
	],
	[
		"icon" => "fa fa-rocket",
		"name" => "PROMOSI ADS",
		"link" => "#",
		"img" => $site_url."assets/img/sosmed2.png",
	],
	[
		"icon" => "fa fa-headphones",
		"name" => "PROMOSI RADIO",
		"link" => "#",
		"img" => $site_url."assets/img/audio1.png",
	],
];

/** VIEW TEMPLATE CONTENT **/
load_template("service/layanan", $data); // "blank" itu dari template/content/blank.php, biar manggilnya gampang wkwk
