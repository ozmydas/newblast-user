<?php

/** QUERY SQL **/
$data["page_title"] = "Service";
$data["subtitle"] = "Layanan";

$data["services"] = [
	[
		"icon" => "fa fa-music",
		"name" => "MIXING MASTERING",
		"link" => "#",
		"img" => $site_url."assets/img/recording1.png",
	],
	[
		"icon" => "fa fa-play",
		"name" => "VIDEO PRODUCTION",
		"link" => "#",
		"img" => $site_url."assets/img/video1.png",
	],
	[
		"icon" => "fa fa-external-link",
		"name" => "LAINNYA",
		"link" => $site_url.'index.php?service-other',
		"img" => $site_url."assets/img/music1.png",
	],
];

/** VIEW TEMPLATE CONTENT **/
load_template("service/layanan", $data); // "blank" itu dari template/content/blank.php, biar manggilnya gampang wkwk
