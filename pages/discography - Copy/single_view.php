<?php

/** QUERY SQL **/
$data["page_title"] = "View Single";
$query = "SELECT releases.*, genres.name as genre, genres.id as genre_id, release_artworks.image, release_songs.lirik, release_songs.file

FROM `releases` 

LEFT JOIN `release_artworks` 
ON `releases`.`id` = `release_artworks`.`release_id` 

LEFT JOIN `release_songs` 
ON `releases`.`id` = `release_songs`.`release_id` 

LEFT JOIN `genres` 
ON `releases`.`genre_id` = `genres`.`id` 

WHERE `releases`.`id` = ".$_GET['id']." AND `releases`.`user_id` = ".$userdata['id'];


$getdata = $database->query($query)->fetchAll();

// var_dump($getdata); exit();

if(empty($getdata)):
	// data kosong
	exit("TES DATA KOSONG");
endif;

// var_dump($getdata[0]); exit();

$data['list_genre'] = $database->select('genres', '*');

if($getdata[0]['label'] == "Musicblast.id"):
	$getdata[0]['price'] = 100000;
else:
	$getdata[0]['price'] = 200000;
endif;

$ada_local_store = FALSE;
$stores = explode(",", $getdata[0]['store']);

foreach ($stores as $key => $value) {
	if($value == " Local Store"):
		$ada_local_store = TRUE;
		break;
	endif;
}
if($ada_local_store):
	$getdata[0]['price'] += 350000;
endif;


$data['single'] = $getdata[0];

/** VIEW TEMPLATE CONTENT **/
load_template("discography/single_view", $data); // "blank" itu dari template/content/blank.php, biar manggilnya gampang wkwk
