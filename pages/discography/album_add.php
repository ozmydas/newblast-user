<?php

/** QUERY SQL **/
$data["page_title"] = "Add Album";
$data['list_genre'] = $database->select('genres', '*');

/** VIEW TEMPLATE CONTENT **/
load_template("discography/album-add", $data);
