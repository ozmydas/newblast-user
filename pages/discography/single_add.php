<?php

/** QUERY SQL **/
$data["page_title"] = "Add Single";
$data['list_genre'] = $database->select('genres', '*');

/** VIEW TEMPLATE CONTENT **/
load_template("discography/single-add", $data);
