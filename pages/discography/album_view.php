<?php

/** QUERY SQL **/
$data["page_title"] = "View Album";

$getdata = $database->select('release_albums', '*', [
	'id_user' => $userdata['id'],
	'id_albums' => $_GET['id'],
]);

// var_dump($getdata); exit();

if(empty($getdata)):
	// data kosong
	exit("TES DATA KOSONG");
endif;

// var_dump($getdata[0]); exit();

$data['album'] = $getdata[0];

/** VIEW TEMPLATE CONTENT **/
load_template("discography/album-view", $data); // "blank" itu dari template/content/blank.php, biar manggilnya gampang wkwk
