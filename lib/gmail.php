<?php

require_once("constant.php");
require_once(dirname(__FILE__)."/../vendor/autoload.php");

class Gugel{
	
	var $client;
	var $database;

	function __construct($db){
		$this->client = new Google_Client();
		$this->database = $db;

		$this->client->setApplicationName('Gmail API Auth Request');
		$this->client->setScopes("https://www.googleapis.com/auth/gmail.compose https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/drive.file");
		$this->client->setAuthConfig(dirname(__FILE__)."/credentials.json");
		$this->client->setAccessType('offline');
		$this->client->setApprovalPrompt('force');
		$this->client->setPrompt('select_account consent');
	} // end func

	function getClient(){
		$tokenPath = 'token.json';
		if (file_exists($tokenPath)):
			$accessToken = json_decode(file_get_contents($tokenPath), true);
			$this->client->setAccessToken($accessToken);
		endif;

		if ($this->client->isAccessTokenExpired()):
			if ($this->client->getRefreshToken()):
				$this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
			else:
				$authUrl = str_replace("promt=select_account", "approval_prompt=auto", $this->client->createAuthUrl());
				return [1, $authUrl, $this->client];
			endif;
		endif;

		return [2, $authUrl, $this->client];
	} // end func


	function getToken($authCode, $mail="master"){
		$accessToken = $this->client->fetchAccessTokenWithAuthCode($authCode);

		if( ! empty($accessToken["error"])):
			return $accessToken["error"];
		endif;

		$this->client->setAccessToken(json_encode($accessToken));

		// save token ke db
		$this->save_token_to_db($accessToken, $mail, @$accessToken["refresh_token"]);
		return null;
	} // end func


	function refresh_token($email="master"){
		// fetch already
		$user = $this->database->select("oauth_google", ["data", "refresh_token"], ["email" => "master", "ORDER" => ["id" => "DESC"]]);
		// var_dump($user);
		$this->client->setAccessToken(str_replace("\/", "/", @$user[0]["data"]));

		// generate new
		$refresh_token = $this->client->getRefreshToken() ? $this->client->getRefreshToken() : str_replace("\/", "/", $user[0]["refresh_token"]);
		$this->client->fetchAccessTokenWithRefreshToken($refresh_token);
		$accessToken = $this->client->getAccessToken();

		if( ! empty($accessToken["error"])):
			return $accessToken;
		endif;

		// save token baru ke client
		$accessToken["refresh_token"] = $refresh_token;
		$this->client->setAccessToken(str_replace("\/", "/", json_encode($accessToken)));
		$this->save_token_to_db($accessToken, $email, $refresh_token);
		return $accessToken;
	} // end func


	function send($sender, $target, $subjek, $raw_message){
		global $GMAIL_ACCOUNT;

		// get saved access token from db
		$user = $this->database->select("oauth_google", "data", ["email" => $sender, "data[~]" => "%access_token%", "ORDER" => ["id" => "DESC"]]);

		// if not found
		if(empty($user[0])):
			return ["status" => false, 'data' => "No Access Token Active Found For ".$sender];
		else:
			// set client access token
			$this->client->setAccessToken(str_replace("\/", "/", $user[0]));
		endif;

		// if kadaluarsa, refresh token
		if ($this->client->isAccessTokenExpired()):
			$this->refresh_token();
		endif;

		// SEND VIA API
		$user_to_impersonate = $GMAIL_ACCOUNT;
		$service = new Google_Service_Gmail($this->client);

		try {
			$strSubject = $subjek;
			$strRawMessage = "From: <".$user_to_impersonate.">\r\n";
			$strRawMessage .= "To: <".$target.">\r\n";
			$strRawMessage .= "Subject: =?utf-8?B?" . base64_encode($strSubject) . "?=\r\n";
			$strRawMessage .= "MIME-Version: 1.0\r\n";
			$strRawMessage .= "Content-Type: text/html; charset=utf-8\r\n";
			$strRawMessage .= "Content-Transfer-Encoding: base64" . "\r\n\r\n";
			$strRawMessage .= $raw_message."\r\n";

			$mime = rtrim(strtr(base64_encode($strRawMessage), '+/', '-_'), '=');
			$msg = new Google_Service_Gmail_Message();
			$msg->setRaw($mime);
			
			$proses = $service->users_messages->send("me", $msg);

			$this->save_mail_log($user_to_impersonate, $target, $strSubject, 1, json_encode($proses));
			return ["status" => true, 'message' => "Email Terkirim", 'data' => $proses];
		} catch (Exception $e) {
			$this->save_mail_log($user_to_impersonate, $target, $strSubject, 0, $e->getMessage());
			return ["status" => false, 'message' => $e->getMessage()];
		}
	} // end func


	/******/


	private function save_token_to_db($accessToken, $email="master", $refresh_token=null){
		// delete first
		$this->database->delete("oauth_google", ["email" => $email]);
		// save token ke db
		$this->database->insert("oauth_google", ["email" => $email, "data" => json_encode($accessToken), "refresh_token" => $refresh_token, "created_datetime" => date("Y-m-d H:i:s")]);
	} // end func

	private function save_mail_log($sender, $recipient, $subject, $status, $response){
		$this->database->insert("log_mail", ["sender" => $sender, "recipient" => $recipient, "subject" => $subject, "status" => $status, "response" => $response, "created_datetime" => date("Y-m-d H:i:s")]);
	} // endfunc

}
