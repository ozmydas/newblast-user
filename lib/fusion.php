<?php
date_default_timezone_set("Asia/Jakarta");
set_time_limit(0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require(dirname(__FILE__)."/../vendor/autoload.php");
require(dirname(__FILE__)."/constant.php");

use Medoo\Medoo;

// Initialize
$database = new Medoo([
	'database_type' => 'mysql',
	'database_name' => $DB_NAME,
	'server' => $DB_HOST,
	'username' => $DB_USER,
	'password' => $DB_PASS
]);


/******/

function base_url($path=""){
	$base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
	$base_url .= "://".$_SERVER['HTTP_HOST'];
	$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

	return $base_url.$path;
} // end func

function post($name){
	if(empty(@$_POST[$name]))
		return NULL;

	return filter_var(@$_POST[$name], FILTER_SANITIZE_STRING);
} // end func

function json_render($data){
	header('Content-Type: application/json');
	echo json_encode($data);

	return true;
} // end func

function render_template($file, $var){
	$files = file_get_contents(dirname(__FILE__)."/../template/".$file);
	$keys = [];

	foreach (array_keys($var) as $key => $value):
		$keys[] = "{{".$value."}}";
	endforeach;

	return str_replace($keys, array_values($var), $files);
} // end func

function load_template($filecontent="", $data=[]){
	if( ! file_exists(dirname(__FILE__)."/../template/html-body.php")):
		return exit("File Template HTML Body Tidak Ditemukan");
	endif;

	if( ! file_exists(dirname(__FILE__)."/../template/content/".$filecontent.".php")):
		return exit("File Template ".$filecontent.".php"." Tidak Ditemukan");
	endif;

	global $site_url, $userdata, $mymessages;
	$maincontent = $filecontent.".php";
	$data = $data;

	if(is_array($data)):
		foreach($data as $key => $value):
			${$key} = $value;
		endforeach;
	endif;

	require(dirname(__FILE__)."/../template/html-body.php");
} // end func

/******/

function curl_post($uri="https://google.co.id", array $data=[]){
	$ch = curl_init($uri);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

// execute!
	$response = curl_exec($ch);

// close the connection, release resources used
	curl_close($ch);

	return $response;
} // end func

/****/

function upfile($config){
	if(empty($_FILES[$config['input']]['name']))
		return ['data' => NULL, 'error' => "Tidak Ada File ".$config['input']." Terdeteksi"];

	if( ! file_exists($config['path']))
		return ['data' => NULL, 'error' => "Target Folder ".$config['path']." Tidak Ditemukan"];

	if( ! empty($config['random'])):
		$extension = pathinfo($_FILES[$config['input']]['name'], PATHINFO_EXTENSION);
		$file_name = date('Ymd').mt_rand(10000000, 99999999).".".$extension;
	else:
		$file_name = $_FILES[$config['input']]["name"];
	endif;

	$temp_name = $_FILES[$config['input']]["tmp_name"];
	$destination = empty($config['path']) ? 'upload' : $config['path'];

	if( ! move_uploaded_file($temp_name, $destination.$file_name)):
		return ['data' => $config, 'error' => "Terjadi Kesalahan ".@$_FILES[$config['input']]["error"]];
	endif;

	return ['data' => ['filename' => $file_name, 'path' => $destination], 'error' => NULL];
} // end func

function upmultifile(){
	return false;
} // end func


/*********************************/


function get_usermail_list($database, $id){
	if(empty($id))
		return [];

	$result = $database->select('pesan', [
		"[>]users" => ["id_pengirim" => "id"],
	], [
		'pesan.id_pesan',
		'pesan.subyek_pesan'
	], [
		'id_penerima' => $id,
		'sudah_dibaca' => 'belum',
		'ORDER' => ['pesan.id_pesan' => 'DESC']
	]);

	return $result;
} // end func

