<?php

require_once("constant.php");
require_once(dirname(__FILE__)."/../vendor/autoload.php");

class Gdrive extends Gugel{

	function __construct($db){
		parent::__construct($db);
	} // end func


	function upload($sender, $filename, $sourcename=null, $tipe=null, $parent_id=null){
		global $GMAIL_ACCOUNT;

		// get saved access token from db
		$user = $this->database->select("oauth_google", "data", ["email" => $sender, "data[~]" => "%access_token%", "ORDER" => ["id" => "DESC"]]);

		// if not found
		if(empty($user[0])):
			return ["status" => false, 'data' => "No Access Token Active Found For ".$sender];
		else:
			// set client access token
			$this->client->setAccessToken(str_replace("\/", "/", $user[0]));
		endif;

		// if kadaluarsa, refresh token
		if ($this->client->isAccessTokenExpired()):
			$this->refresh_token();
		endif;

		$tipe = ! empty($tipe) ? $tipe : 'image/jpeg'; 
		$sourcename = empty($sourcename) ? $filename : $sourcename;
		$drive_options = [
			'name' => $filename
		];

		if( ! empty($parent_id))
			$drive_options['parents'] = array($parent_id);

		// SEND VIA API
		$user_to_impersonate = $GMAIL_ACCOUNT;

		$service = new Google_Service_Drive($this->client);
		$fileMetadata = new Google_Service_Drive_DriveFile($drive_options);

		$content = file_get_contents($sourcename);
		$file = $service->files->create($fileMetadata, array(
			'data' => $content,
			'mimeType' => $tipe,
			'uploadType' => 'multipart',
			'fields' => 'id'));

		return $file;
	} // end func



}