<?php

function datatable(array $input,array $where_rules=[], $pkey, array $join_rules=[], bool $is_debug=false){
	$table = $input["table"];
	$column = $input["column"];
	$column_orderable = $input["column_orderable"];
	$primary_key = key($pkey);
	$limit = 10;
	$offset = 0;
	$order_offset = "";
	$order_direction = "";

	if(isset($_POST["start"])):
		$offset = $_POST["start"];
	endif;

	if(isset($_POST["length"])):
		$limit = $_POST["length"];
	endif;

	$rules_count = [];

	if( ! empty($where_rules)):
		foreach ($where_rules as $key => $value):	
			$rules[$key] = $value;
			$rules_count[$key] = $value;
		endforeach;	
	endif;

	$rules["LIMIT"] = [$offset, $limit];
	$rules["ORDER"] = [$primary_key => $pkey[$primary_key]];


	if(! is_null(@$_POST["order"][0]["column"]) AND ! empty($_POST["order"][0]["dir"])):
		$order_offset = @$_POST["order"][0]["column"];
	$order_direction = @$_POST["order"][0]["dir"];

	$rules["ORDER"] = [$column_orderable[$order_offset] => strtoupper($order_direction)];
endif;

if(! empty($_POST["search"]["value"])):
	foreach ($input["column_search"] as $key => $value):
		$rules["OR"][$value."[~]"] = $_POST["search"]["value"];
		$rules_count["OR"][$value."[~]"] = $_POST["search"]["value"];
	endforeach;
endif;

global $database;

$jumlah = $database->count($table, $rules_count);

if(empty($join_rules)):
	if($is_debug):
		$jumlah = $database->debug()->count($table, $rules_count);
		return $database->debug()->select($table, $column, $rules);
	else:
		$jumlah = $database->count($table, $rules_count);
		$data = $database->select($table, $column, $rules);
	endif;
else:
	if($is_debug):
		$jumlah = $database->debug()->count($table, $join_rules, $primary_key, $rules_count);
		return $database->debug()->select($table, $join_rules, $column, $rules);
	else:
		$jumlah = $database->count($table, $join_rules, $primary_key, $rules_count);
		$data = $database->select($table, $join_rules, $column, $rules);
	endif;
endif;

return [
	"data" => $data,
	"jumlah" => $jumlah,
];
}