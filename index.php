<?php session_start();


require(dirname(__FILE__)."/lib/fusion.php");


/** VALIDASI SESI USER START **/

// dummy session biar gampang debug
// $_SESSION["userdata"] = [
// 	"id" => 120,
// 	"name" => "Vannudin",
// 	"email" => "vannudin@gmail.com",
// 	"token" => sha1(rand()),
// ];


if(empty($_SESSION['userdata']['id'])):
	$login = base_url("login.php");
	header("Location: ".$login);
	exit();
endif;

global $userdata, $mymessages;
$userdata = $_SESSION["userdata"];

/** TAMBAHAN **/

$mymessages = get_usermail_list($database, $userdata['id']);


/** DAFTAR LIST URL (YANG DIDALEM FOLDER PAGES) **/

$routes = [
	"dashboard" => "home/dashboard",
	"profil" => "user/profile",
	"single" => "discography/single",
	"add_single" => "discography/single_add",
	"view_single" => "discography/single_view",
	"album" => "discography/album",
	"add_album" => "discography/album_add",
	"view_album" => "discography/album_view",
	"storelist" => "service/storelist",
	"message" => "message/compose",
	"layanan" => "service/layanan",
	"promotion" => "service/promotion",
	"service" => "service/service",
	"service-other" => "service/service_lainnya",
	"serviceitem" => "service/item",
	"report" => "service/report",
	"payment" => "order/payment",
	"info" => "home/info",
	"tes" => "home/tes",
];

$site_url = base_url();
$notfound_page = "/pages/home/404.php";

foreach($routes as $key => $value):
	if(isset($_GET[$key])):
		$file_to_load = dirname(__FILE__)."/pages/".$value.".php";

		if(file_exists($file_to_load)):
			require($file_to_load);
		else:
			$component_err = $key;
			require(dirname(__FILE__).$notfound_page);
		endif;

		return true;
	endif;
endforeach;


/** JIKA TIDAK MEMBUKA URL YG DIATAS, BALIK KE LOGIN **/

require(dirname(__FILE__).$notfound_page);

// $login = base_url("login.php");
// header("Location: ".$login);